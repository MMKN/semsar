<?php

Route::get('/fb' , 'HomeController@loginWithFacebook');
Route::get('/property/{id}' , [
    'as' => 'property',
    'uses' => 'PropertiesController@property'
]);

Route::controller('/profile' , 'ProfileController',
    [
        'getIndex'              => 'profile.main',
        'getProperty'           => 'profile.property',
        'getGird'               => 'profile.grid',
        'getNotifications'      => 'profile.notifications'
    ]
);

Route::controller('/' , 'PropertiesController' ,
    [
        'getIndex'      => 'properties.main',
        'postFilter'    => 'properties.filter',
        'postReset'     => 'filter.reset',
        'postSave'      => 'filter.save',
        'getNeighbour'  => 'properties.neighbours',
        'getProperty'   => 'properties.property',
        'getGrid'       => 'properties.grid',
        'getPaginated'  => 'properties.paginated',

        'getProfile'    => 'profile.main',

        'postRequest'   => 'property.request',
        'postFavourite' => 'property.favourite'
    ]
);
