<?php

class ProfileController extends BaseController {

    public function getIndex(){
        if (!Session::get('id')) {
            return Redirect::route('properties.main');
        }
		$data = $this->getProfile();
        $data['unread_notifications'] = Notification::whereUser_id(Session::get('id'))->whereRead(0)->get();
        // print_r(count($data['unread_notifications']));
        // dd();
        $data['notifications'] = Notification::latest()->whereUser_id(Session::get('id'))->get();        
        return View::make('profile.main' , $data);
	}

    public function getProfile(){
        $user_id      = Session::get('id'); // form sessions
        $data['user'] = PublicUser::find($user_id);
        $favourites   = Property::whereIn('id' , Favourite::whereUser_id($user_id)->lists('property_id'))->get();
        $properties   = $this->matchProperties($user_id);
        $data['properties']       = $properties['property'];
        $data['suggestions']      = $properties['suggestions'];
        $data['price_properties'] = $properties['price_properties'];
        $data['favourites']       = $favourites;
        return $data;
    }

    public function getPrefs(){
    	$user_id      = Session::get('id');
    	$user = PublicUser::find($user_id);
    	$data['user'] = $user;
    	$data['selected_neighbours'] = Neighbour::whereCity_id($user->city)->get();
    	return View::make('profile.filter' , $data);
    }

    public function postStore(){

    	$user_id = Session::get('id');
    	$user = PublicUser::find($user_id);
    	$user->name   = Input::get('name');
    	$user->mobile = str_replace(" " , "" , str_replace("-" , "" , Input::get('mobile')));
    	if($user->save()){
    		return 1;
    	}else{
    		return App::abort(500, 'Error occured... please try again later');
    	}
    }


    public function getProperty(){
        $property_id = Input::get('property_id');
        $data['f'] = 0; // to detremine if add to favourite or remove
        if (Session::get('id')){
            $fav = Favourite::whereProperty_id($property_id)->whereUser_id(Session::get('id'))->get()->first();
            if($fav){ $data['f'] = 1; }
        }

    	$data['property'] = Property::whereId($property_id)->get()->first();
        return Response::json(array(
			'result'    =>  (String)View::make('properties.property' , $data)
		));
    }

    public function postFavourite(){
        $property = new PropertiesController();
        $fav      = $property->postFavourite();
        return $fav;
    }

    public function postRequest(){
        $property = new PropertiesController();
        $fav      = $property->postRequest();
        return $fav;
    }

    public function postSubmitMobile(){
        $id = Session::get('id');
        $user = PublicUser::find($id);
        $user->mobile = str_replace(" " , "" , str_replace("-" , "" , Input::get('mobile')));
        $user->save();
    }

    public function getGrid(){
        $data = $this->getProfile();
        return View::make('profile.grid' , $data);
    }

    /**
     * matchProperties
     *
     * returns collection of matched properties
     *
     * @param (int) user id to match with
     * @return (Collection)
     */
    private function matchProperties($user_id = 0){
    	$user = PublicUser::whereId($user_id)->first();
        $property         = new Property();
        $suggestions      = new Property();
        $price_properties = new Property();

        $price_properties = $price_properties->whereCity($user->city);
        $property         = $property->whereCity($user->city);
        $suggestions      = $suggestions->whereCity($user->city);

        $property         = $property->wherePurpose($user->purpose);
        $suggestions      = $suggestions->wherePurpose($user->purpose);
        $price_properties = $price_properties->wherePurpose($user->purpose);

        if ($user->price_from != 0) {
            $property         = $property->where('price' , '>=' , $user->price_from);
            $suggestions      = $suggestions->where('price' , '>=' , $user->price_from);
            $price_properties = $price_properties->where('price' , '>=' , $user->price_from);
        }
        
        if ($user->price_to != 0) {
            $property         = $property->where('price' , '<=' , $user->price_to);
            $suggestions      = $suggestions->where('price' , '<=' , $user->price_to);
            $price_properties = $price_properties->where('price' , '<=' , $user->price_to);
        }
        
        if($user->objective != ""){
	        $property    = $property->whereObjective_id($user->objective);
	        $suggestions = $suggestions->whereObjective_id($user->objective);
        }

        if($user->is_compound != ""){
	        $property         = $property->whereIs_compound($user->is_compound);
	        $suggestions      = $suggestions->whereIs_compound($user->is_compound);
        }
        
        if ($user->unit_type != "") {
            $property = $property->where(function($query) use ($user){
                $query->whereIn('unit_type' , explode("|" , $user->unit_type));
            });
            $suggestions = $suggestions->where(function($query) use ($user){
                $query->whereIn('unit_type' , explode("|" , $user->unit_type));
            });
        }
        
        if ($user->payment_style != "") {
            $property = $property->where(function($query) use ($user){
                $query->whereIn('payment_type' , explode("|" , $user->payment_type));
            });
            $suggestions = $suggestions->where(function($query) use ($user){
                $query->whereIn('payment_type' , explode("|" , $user->payment_type));
            });
        }

        if ($user->finishing != "") {
            $property = $property->where(function($query) use ($user){
                $query->whereIn('finishing' , explode("|" , $user->finishing));
            });
            $suggestions = $suggestions->where(function($query) use ($user){
                $query->whereIn('finishing' , explode("|" , $user->finishing));
            });
        }
            
        if($user->neighbour != ""){
        	$property = $property->where(function($query) use ($user){
        	    $query->whereIn('neighbour' , explode("|" , $user->neighbour));
        	});
	        $group_array      = [];
	        $neighbours_array = [];
	        foreach ( explode("|" , $user->neighbour) as $neighbour) {
	            $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
	            foreach ($group as $g) {
	                array_push($group_array , $g->group_id);
	            }
	        }

	        $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
	        foreach ($neighbours as $n) {
	            array_push($neighbours_array , $n->neighbour_id);
	        }

	        $neighbours_array = array_unique($neighbours_array);

	        $suggestions = $suggestions->where(function($query) use ($neighbours_array){
	            $query->whereIn('neighbour' , $neighbours_array);
	        });
    	}	
    	
    	if($user->floor != ""){
            $property = $property->where(function($query) use ($user){
                $query->whereIn('floor' ,  explode("|" , $user->floor));
            });
	        $group_array  = [];
	        $floors_array = [];
	        foreach (explode("|" , $user->floor) as $floor) {
	            $floor = FloorGroup::whereFloor_id($floor)->get();
	            foreach ($floor as $f) {
	                array_push($group_array , $f->group_id);
	            }
	        }

	        $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
	        foreach ($floors as $f) {
	            array_push($floors_array , $f->floor_id);
	        }

	        $floors_array = array_unique($floors_array);

	        $suggestions = $suggestions->where(function($query) use ($floors_array){
	            $query->whereIn('floor' , $floors_array);
	        });
    	}
    	   
        $data['property']         = $property->get();
        $data['suggestions']      = $suggestions->get();
        $data['price_properties'] = $price_properties->get();

        $data['suggestions']      = $data['suggestions']->diff($data['property']);
        $data['price_properties'] = $data['price_properties']->diff($data['property']);
        $data['price_properties'] = $data['price_properties']->diff($data['suggestions']);

        if (!$user->price_form && !$user->price_to) {
            $data['price_properties'] = [];
        }

        return $data;
    }


    public function getNotifications()
    {
        if (!Session::get('id')){ return Redirect::to('/'); }

        $id = Session::get('id');
        // $id = 10206997135900037;
        $all_notifications      = Notification::whereUser_id($id);
        $data['notifications']  = $all_notifications->get();

        $month_notifications    = $all_notifications->where('created_at' , '>=' , date( 'Y-m-j' , strtotime("-1 months")));
        $data['month']          = $month_notifications->get();

        $yesteday_notifications = $month_notifications->where('created_at' , '>=' , date( 'Y-m-j' , strtotime("-1 days")));
        $data['yesterday']      = $yesteday_notifications->get();

        // dd($data['yesterday']);

        $today_notifications    = $yesteday_notifications->where('created_at' , '>=' , date( 'Y-m-j' , strtotime("today")));
        $data['today'] = $today_notifications->get();

        $data['notifications'] = $data['notifications']->diff($data['month']);
        $data['month']         = $data['month']->diff($data['yesterday']);
        $data['yesterday']     = $data['yesterday']->diff($data['today']);
        return View::make('profile.notification' , $data);
    }

    public function getNotificationFilter(){
    	$notifications = new Notification();

    	if(Input::has('status')){
    		$notifications = $notifications->whereRead(Input::get('status'));
    	}
    	if(Input::has('start_date')){
    		$notifications = $notifications->where('created_at' , '>=' , date('Y-m-d' , strtotime(Input::get('start_date'))));
    	}
    	if(Input::has('end_date')){
    		$notifications = $notifications->where('created_at' , '<=' , date('Y-m-d' , strtotime(Input::get('end_date'))));
    	}

    	$data['notifications'] = $notifications->get();

    	return View::make('profile.notification_filter' , $data);
    }
}
