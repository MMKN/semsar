<?php

// use OAuth;

class HomeController extends BaseController {

	// // get login page
	// public function getLogin()
	// {
	// 	if(Auth::user()->check()){
	// 		return Redirect::route('properties.index');
	// 	}
	// 	return View::make('login');
	// }
	//
	// // handle login page submit
	// public function postLogin()
	// {
	// 	$email    = Input::get('email');
	// 	$password = Input::get('password');
	//
	// 	if(Auth::user()->attempt(array('email' => $email , 'password' => $password))){
	// 		$user = Auth::user()->user();
	// 		if($user->image == ""){
	// 			Session::put('image'     , 'user.png');
	// 		}else{
	// 			Session::put('image'     , $user->image);
	// 		}
	//
	// 		$data['message'] = "You Are Logged In Successfully.";
	// 		return Response::json(array(
	// 			'url'      => URL::route('properties.index'),
	// 			'message'  => (String)View::make('success_message' , $data),
	// 			'state'    => 'success'
	// 		));
	// 	}
	//
	// 	$data['message'] = "Wrong Email/Password.";
	// 	return Response::json(array(
	// 		'message' => (String)View::make('danger_message' , $data),
	// 		'state'   => 'fail'
	// 	));
	// }
	//
	// public function postLogout()
	// {
	// 	Auth::user()->logout();
	// 	return Redirect::to('/');
	// }
	//
	// public function getCustomerLogin(){
	// 	return View::make('customers_login');
	// }
	//
	// public function postCustomerLogin()
	// {
	// 	$email    = Input::get('email');
	// 	$password = Input::get('password');
	// 	if(Auth::customer()->attempt(array('username' => $email , 'password' => $password))){
	// 		$customer = Auth::customer()->user();
	// 		Session::put('id' , $customer->id);
	// 		$data['message'] = "You Are Logged In Successfully.";
	// 		return Response::json(array(
	// 			'url'      => URL::route('account'),
	// 			'message'  => (String)View::make('success_message' , $data),
	// 			'state'    => 'success'
	// 		));
	// 	}
	//
	// 	$data['message'] = "Wrong Email/Password.";
	// 	return Response::json(array(
	// 		'message' => (String)View::make('danger_message' , $data),
	// 		'state'   => 'fail'
	// 	));
	// }
	//
	// public function postCustomerLogout()
	// {
	// 	Auth::customer()->logout();
	// 	return Redirect::to('/login');
	// }

	public function loginWithFacebook() {
        // User::whereId($userid);
        if(Auth::check()){
            return Redirect::to('/');
        }
        // get data from input
        $code = Input::get( 'code' );
        // get fb service
        $fb = OAuth::consumer( 'Facebook' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken( $code );
            // dd($token);
            // Send a request with it
            $me = json_decode($fb->request( '/me?fields=id,email,first_name,last_name,picture,name' ), true );
			$exsist = PublicUser::find($me['id']);
			if(!$exsist){ // sign up
				$user        = new PublicUser();
				$user->id    = $me['id'];
				$user->name  = $me['name'];
				$user->email = $me['email'];
				$user->url   = 'https://graph.facebook.com/'. $me['id'] .'/picture?type=large';
				$user->save();
				Session::put('id' , $me['id']);
				Session::put('name' , $me['name']);
				Session::put('email' , $me['email']);
			}else{ // sign in
				Session::put('id' , $me['id']);
				Session::put('name' , $me['name']);
				Session::put('email' , $me['email']);
				// dd('signed up already');
			}
            // $message = 'Your unique facebook user id is: ' . $me['id'] . ' and your name is ' . $me['name'];
            // echo $message. "<br/>";

			return Redirect::route('properties.main');

            $uid = $me['id'];

            $profile = Profile::whereUid($uid)->first();

            if (empty($profile)) {
                if(!array_key_exists('email', $me)){
                    return Redirect::to('/login')->with('errorMsg','Error: No email associated with your facebook account. Please register using your email');
                }

                if(User::whereEmail($me['email'])->exists()){
                    $user = User::whereEmail($me['email'])->first();
                }else{
                    $user = new User;
                    $user->first_name = $me['first_name'];
                    $user->last_name = $me['last_name'];
                    $user->email = $me['email'];
                    $user->image = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';
                    $user->save();
                }


                $profile = new Profile();
                $profile->uid = $uid;
                $profile->username = $me['first_name']." ".$me['last_name'];
                $profile = $user->profile()->save($profile);

            }else{
                $user = User::find($profile['user_id']);
                Auth::user()->login($user);
                return Redirect::to('/')->with('message', 'Logged in with Facebook');
            }

            // $profile->access_token = $fb->getAccessToken();
            // $profile->save();

            // $user = $profile->user;
            $user = User::find($profile->user_id);
            Auth::user()->login($user);

            return Redirect::to('/')->with('message', 'Logged in with Facebook');

        }
        // if not ask for permission first
        else {
            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
             return Redirect::to( (string)$url );
        }

    }
}
