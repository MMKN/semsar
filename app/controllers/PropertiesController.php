<?php

class PropertiesController extends BaseController {


    public function getIndex(){
        $data['user'] = 0;
        if (Session::get('id')) {
            $data['user'] = 1;
        }
        $data['properties'] = Property::latest()->paginate(20);
        $data['unread_notifications'] = Notification::whereUser_id(Session::get('id'))->whereRead(0)->get();
        // print_r(count($data['unread_notifications']));
        // dd();
        $data['notifications'] = Notification::latest()->whereUser_id(Session::get('id'))->get();
        // dd($data['notifications']->toArray());
        return View::make('properties.main' , $data);
    }

    public function getPaginated(){
        $data['properties'] = Property::orderBy('created_at' , 'desc')->paginate(20);
        $properties = Property::orderBy('created_at' , 'desc')->paginate(20);
        return (String)View::make('properties.main_grid' , $data);
    }

    public function getNeighbour(){
        $city_id            = Input::get('city_id');
        $data['neighbours'] = Neighbour::whereCity_id($city_id)->get();
        return Response::json(array(
			'result'    =>  (String)View::make('properties.neighbours' , $data),
		));
    }

    public function postFilter(){
        $property         = new Property();  // Exact Match
        $suggestions      = new Property();  // Suggested Properties
        $price_properties = new Property();  // Properties with the same price

        // check if value exists or not
        // If exists filter by value for both
        if(Input::has('purpose')){
            $property         = $property->wherePurpose(Input::get('purpose'));
            $suggestions      = $suggestions->wherePurpose(Input::get('purpose'));
            $price_properties = $price_properties->wherePurpose(Input::get('purpose'));
        }

        if(Input::has('unit_type') && Input::get('unit_type')[0]){
            $property = $property->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });

            $price_properties = $price_properties->where(function($query){
                $query->whereIn('unit_type' , Input::get('unit_type'));
            });
        }

        if(Input::has('city')){
            $property            = $property->whereCity(Input::get('city'));
        }

        if(Input::has('neighbour') && Input::get('neighbour')[0]){
            $property = $property->where(function($query){
                $query->whereIn('neighbour' , Input::get('neighbour'));
            });

            // store all the groups of the neighbours sent by the user
            $group_array      = [];
            $neighbours_array = [];
            foreach (Input::get('neighbour') as $neighbour) {
                $group = NeighbourGroup::whereNeighbour_id($neighbour)->get();
                foreach ($group as $g) {
                    array_push($group_array , $g->group_id);
                }
            }

            $neighbours = NeighbourGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($neighbours as $n) {
                array_push($neighbours_array , $n->neighbour_id);
            }

            $neighbours_array = array_unique($neighbours_array);

            $suggestions = $suggestions->where(function($query) use ($neighbours_array){
                $query->whereIn('neighbour' , $neighbours_array);
            });
        }

        if(Input::has('price_from')){
            $property         = $property->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
            $suggestions      = $suggestions->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
            $price_properties = $price_properties->where('price' , '>=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))));
        }

        if(Input::has('price_to')){
            $property         = $property->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
            $suggestions      = $suggestions->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
            $price_properties = $price_properties->where('price' , '<=' , str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))));
        }

        // Fetch Margin values before matching
        if(Input::has('area')){
            $margin = Margin::whereField_name('area')->pluck('value_exact');
            if (!$margin){
                $margin = 10;
            }
            $margin      = Input::get('area') * ($margin/100);
            $property    = $property->whereBetween('area' , [ (int)str_replace("," , "" , Input::get('area')) - $margin  , (int)str_replace("," , "" , Input::get('area')) + $margin ]);

            $margin_s = Margin::whereField_name('area')->pluck('value_suggest');
            if (!$margin_s){
                $margin_s = 20;
            }
            $margin_s    = Input::get('area') * ($margin_s/100);
            $suggestions = $suggestions->whereBetween('area' , [ (int)str_replace("," , "" , Input::get('area')) - $margin_s  , (int)str_replace("," , "" , Input::get('area')) + $margin_s  ]);
        }

        if(Input::has('floor') && Input::get('floor')[0]){
            $property = $property->where(function($query){
                $query->whereIn('floor' , Input::get('floor'));
            });

            // store all the groups of the neighbours sent by the user
            $group_array  = [];
            $floors_array = [];
            foreach (Input::get('floor') as $floor) {
                $floor = FloorGroup::whereFloor_id($floor)->get();
                foreach ($floor as $f) {
                    array_push($group_array , $f->group_id);
                }
            }

            $floors = FloorGroup::whereIn('group_id' , array_unique($group_array))->get();
            foreach ($floors as $f) {
                array_push($floors_array , $f->floor_id);
            }

            $floors_array = array_unique($floors_array);

            $suggestions = $suggestions->where(function($query) use ($floors_array){
                $query->whereIn('floor' , $floors_array);
            });
        }

        if(Input::has('objective')){
            $property    = $property->whereObjective_id(Input::get('objective'));
            $suggestions = $suggestions->whereObjective_id(Input::get('objective'));
        }

        if(Input::has('payment_style') && Input::get('payment_style')[0]){
            $property = $property->where(function($query){
                $query->whereIn('payment_style' , Input::get('payment_style'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('payment_style' , Input::get('payment_style'));
            });
        }

        if(Input::has('finishing') && Input::get('finishing')[0]){
            $property = $property->where(function($query){
                $query->whereIn('finishing' , Input::get('finishing'));
            });

            $suggestions = $suggestions->where(function($query){
                $query->whereIn('finishing' , Input::get('finishing'));
            });
        }

        if(Input::has('compound')){
            $property    = $property->whereIs_compound(1);
            $suggestions = $suggestions->whereIs_compound(1);
        }else{
            $property    = $property->whereIs_compound(0);
            $suggestions = $suggestions->whereIs_compound(0);
        }

        $property         = $property->orderBy('created_at' , 'desc')->get();
        $suggestions      = $suggestions->orderBy('created_at' , 'desc')->get();
        $price_properties = $price_properties->orderBy('created_at' , 'desc')->get();
        $suggestions      = $suggestions->diff($property);
        $price_properties = $price_properties->diff($property);
        $price_properties = $price_properties->diff($suggestions);

        // if the price isn't set return empty for the [price properties] \\
        if (!Input::has('price_from') && !Input::has('price_to')) {
            $price_properties = [];
        }

        $data['properties']       = $property;
        $data['suggestions']      = $suggestions;
        $data['price_properties'] = $price_properties;

        return Response::json(array(
			'result'    =>  (String)View::make('properties.filter_grid' , $data),
		));
    }

    public function postSave(){
        if (!Session::get('id')) {
            return 2;
        }
        $user_id = Session::get('id');
        $user = PublicUser::find($user_id);

        $user->purpose      = Input::get('purpose')       ? Input::get('purpose') : '';
        $user->unit_type    = Input::get('unit_type')     ? "|" . implode('|' , Input::get('unit_type')) . "|" : '';
        $user->city         = Input::get('city')          ? Input::get('city') : '';
        $user->neighbour    = Input::get('neighbour')     ? "|" . implode('|' , Input::get('neighbour')) . "|" : '';
        $user->price_from   = Input::get('price_from')    ? str_replace("$" , "" , str_replace("," , "" , Input::get('price_from'))) : '';
        $user->price_to     = Input::get('price_to')      ? str_replace("$" , "" , str_replace("," , "" , Input::get('price_to'))) : '';
        $user->area         = Input::get('area')          ? str_replace("," , "" , Input::get('area')) : '';
        $user->floor        = Input::get('floor')         ? "|" . implode('|' , Input::get('floor')) . "|" : '';
        $user->objective    = Input::get('objective')     ? Input::get('objective') : '';
        $user->payment_type = Input::get('payment_style') ? "|" . implode('|' , Input::get('payment_style')) . "|" : '';
        $user->finishing    = Input::get('finishing')     ? "|" . implode('|' , Input::get('finishing')) . "|" : '';
        $user->is_compound  = Input::get('compound')      ? 1 : 0;

        $user->update();
    }

    public function postReset(){
        $data['properties'] = Property::orderBy('created_at' , 'desc')->paginate(20);
        return Response::json(array(
			'result'    =>  (String)View::make('properties.main_grid' , $data),
		));
    }

    public function getProperty(){
        $property_id = Input::get('property_id');
        $data['f'] = 0; // to detremine if add to favourite or remove
        if (Session::get('id')){
            $fav = Favourite::whereProperty_id($property_id)->whereUser_id(Session::get('id'))->get()->first();
            if($fav){ $data['f'] = 1; }
        }

    	$data['property'] = Property::whereId($property_id)->get()->first();
        return Response::json(array(
			'result'    =>  (String)View::make('properties.property' , $data)
		));
    }

    public function getGrid(){
        return $this->postReset();
    }

    public function postRequest(){
        if (!Session::get('id')) {
            return 2;
        }

        $user_id = Session::get('id');
        $mobile  = PublicUser::find($user_id)->mobile;

        if (!$mobile) {
            return 4;
        }

        $property_id = Input::get('property_id');
        $exsist      = AdminNotification::whereType(1)->whereUser_id($user_id)->whereProperty_id($property_id)->get()->first();
        if($exsist) return 0;
        $request              = new AdminNotification();
        $request->title       = 'User ' . PublicUser::find($user_id)->name . ' has request a call for a property.';
        $request->type        = 1;
        $request->key         = $request->generate_random_string(7);
        $request->property_id = $property_id;
        $request->user_id     = $user_id;
        $request->save();
        return 1;
    }

    public function postSubmitMobile(){
        $id = Session::get('id');
        $user = PublicUser::find($id);
        $user->mobile = str_replace(" " , "" , str_replace("-" , "" , Input::get('mobile')));
        $user->save();
    }

    public function postFavourite(){
        if (!Session::get('id')) {
            return 2;
        }
        $property_id = Input::get('property_id');
        $user_id     = Session::get('id');
        $exsist = Favourite::whereUser_id($user_id)->whereProperty_id($property_id)->get()->first();

        if($exsist){
            $exsist->delete();
            return 0;
        }
        $favourite = new Favourite();
        $favourite->property_id = $property_id;
        $favourite->user_id     = $user_id;
        $favourite->save();
        return 1;
    }

    public function property($id)
    {
        $notification = Notification::whereProperty_id($id)->whereUser_id(Session::get('id'))->get()->first();
        if ($notification){
            $notification->read = 1;
            $notification->update();
        }else{
            return Redirect::to('/');
        }
        $data['f'] = 0; // to detremine if add to favourite or remove
        if (Session::get('id')){
            $fav = Favourite::whereProperty_id($id)->whereUser_id(Session::get('id'))->get()->first();
            if($fav){ $data['f'] = 1; }
        }

        $data['property'] = Property::find($id);
        return View::make('property.property' , $data);
    }

    public function getPrices(){
        if(Input::get('purpose') == '0'){
            // return (String)View::make('properties.sale_price');
            return Response::json([
                'from' => (String)View::make('properties.sale_price'),
                'to'   => (String)$this->getTo()
            ]);
        }else{
            // return (String)View::make('properties.rent_price');
            return Response::json([
                'from' => (String)View::make('properties.rent_price'),
                'to'   => (String)$this->getTo()
            ]);
        }
    }

    public function getTo(){
        $sale_prices = [100000
                        ,125000
                        ,150000
                        ,175000
                        ,200000
                        ,250000
                        ,300000
                        ,350000
                        ,400000
                        ,450000
                        ,500000
                        ,600000
                        ,700000
                        ,800000
                        ,900000
                        ,1000000
                        ,1200000
                        ,1400000
                        ,1600000
                        ,1800000
                        ,2000000
                        ,2300000
                        ,2600000
                        ,3000000
                        ,3500000
                        ,4000000
                        ,5000000
                        ,6000000
                        ,7500000
                        ,10000000
                        ,15000000
                        ,20000000];
        $rent_prices = [1000
                        ,1500
                        ,2000
                        ,3000
                        ,5000
                        ,7000
                        ,15000
                        ,20000];
        
        if(Input::get('purpose') == '0'){
            $price_start = array_search(Input::get('price') , $sale_prices);
            $data['prices'] = array_slice($sale_prices , $price_start +1);
            return View::make('properties.prices_to' , $data);
        }else{
            $price_start = array_search(Input::get('price') , $rent_prices);
            $data['prices'] = array_slice($rent_prices , $price_start +1);
            return View::make('properties.prices_to' , $data);
        }
    }
        
}
