<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */


	public function __construct(){

		/*
		*  Global Variable Declaration
		*  Author: Sherif Muhammad
		*  Date: 17/10/2015
		*/

   		App::setLocale(Language::whereSelected(1)->get()->first()->value);
   		App::setLocale('ar');

		$data['unit_types']    = UnitType::all();
		$data['cities']        = City::all();
		$data['neighbours']    = Neighbour::all();
		$data['payment_types'] = PaymentType::all();
		$data['finishings']    = Finishing::all();
		$data['features']      = Feature::all();
		$data['floors']        = Floor::all();
		$data['objectives']    = Objective::all();
		$data['groups']        = Group::all();
		$data['languages']     = Language::all();
		// $data['margins']        = Margin::all();

		View::share('unit_types' , $data['unit_types']);
		View::share('cities' , $data['cities']);
		View::share('neighbours' , $data['neighbours']);
		View::share('payment_types' , $data['payment_types']);
		View::share('finishings' , $data['finishings']);
		View::share('features' , $data['features']);
		View::share('floors' , $data['floors']);
		View::share('objectives' , $data['objectives']);
		View::share('groups' , $data['groups']);
		View::share('languages' , $data['languages']);
		// View::share('margins' , $data['margins']);
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
