<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">
    <title>Semsar Online</title>
    <!-- uikit -->
    <link rel="stylesheet" href="../bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <!-- flag icons -->
    <link rel="stylesheet" href="../assets/icons/flags/flags.min.css" media="all">
    <!-- altair admin -->
    <link rel="stylesheet" href="../assets/css/main.min.css" media="all">
    <link rel="stylesheet" href="{{ URL::asset('assets/js/lightbox2-master/src/css/lightbox.css') }}" media="all">
    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->

</head>
<body class="top_menu">

    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <!-- <div class="main_logo_top">
                    <a href="index.html"><img src="assets/img/white.png" alt=""     height="15" width="71"/></a>
                </div > -->
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->

                <!-- 
                    <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                            <a href="#" class="top_menu_toggle"><i class="material-icons uk-margin-large-right md-24">info</i><i class="material-icons md-24">call</i> 01123777760</a>

                            <div class="uk-dropdown uk-dropdown-width-3">
                                <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                    <div class="uk-width-1-2">
                                        <div class="uk-grid uk-container-center uk-grid-width-medium-1-2 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                           
                                            <a href="http://minionz.net" target="_blank">
                                                <span class="uk-text-muted uk-display-block uk-margin-bottom">Developed by</span>
                                                <img src="assets/img/favicon.png" alt=""     height="15" width="71"/>
                                                <span class="uk-text-muted uk-display-block uk-margin">Minionz</span>

                                            </a>
                                            
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <ul class="uk-nav uk-nav-dropdown uk-panel">
                                            <li class="uk-nav-header">وسائل الإتصال</li>
                                            <li><a >01123777760</a></li>
                                            <li><a >01149934994</a></li>
                                            <li><a >01149935993</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
 -->
                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons uk-margin-large-right md-24">info</i><i class="material-icons md-24">call</i> 01123777760</a>

                        <div class="uk-dropdown uk-dropdown-width-1">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-1-1">
                                    <div class="uk-grid  uk-grid-width-medium-1-1 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                       
                                        <a href="http://minionz.net" target="_blank">
                                            <span class="uk-text-muted uk-display-block uk-margin-bottom">Developed by</span>
                                            <img src="assets/img/favicon.png" alt=""     height="15" width="71"/>
                                            <span class="uk-text-muted uk-display-block uk-margin">Minionz</span>
                                            

                                        </a>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                @if(Session::get('id'))
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-warning" href="{{ URL::route('profile.main') }}" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> {{trans('main.my_account')}}</a>
                </div>
                @else
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-primary" href="fb" id="fb_button" style="margin-top: 6px;"><i class="uk-icon-facebook uk-icon-small" style="color:#fff"></i> {{trans('main.sing_in')}}</a>
                </div>
                @endif

</nav>
</div>

</header><!-- main header end -->

<div id="page_content">

    <!-- properties container start -->
    <div id="page_content_inner" class="hierarchical_show" onscroll="scrolldetect()">

        <!-- property head begin -->
        <div class=" uk-width-10-10 uk-container-center user_heading">

            <div class="user_heading_avatar" style="margin-top:13px">
                <i class="material-icons md-48 md-light uk-text-middle">&#xE88A;</i>
            </div>
            <div class="user_heading_content">
                <h2 class="heading_b uk-margin-bottom uk-float-left "><span class="uk-text-truncate">{{ $property->getUnitType($property->unit_type) }}</span><span class="sub-heading">{{ $property->id }}</span></h2>
                <ul class="user_stats uk-float-right">
                    <li class="">
                        <a onclick="add_request({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">call</i> Request a Call</a>
                    </li>

                    @if(!$f)
                    <li>
                        <a onclick="add_remove_favourite({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">star</i> <span id="favourite_text"> {{trans('main.Add_to_fav')}} </span></a>
                    </li>
                    @else
                    <li>
                        <a onclick="add_remove_favourite({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">star</i> <span id="favourite_text"> {{trans('main.remove_fav')}} </span></a>
                    </li>
                    @endif

                </ul>
            </div>
        </div>
        <!-- property head end -->



        <div class="uk-grid">
            <div class="uk-width-large-5-10">
                <div class="md-card user_content">


                    <!-- property images begin -->
                    <h4 class="full_width_in_card heading_c">Images</h4>
                    <div id="" class="uk-grid" >
                        @foreach($property->getImages($property->id) as $image)
                        <!-- image sample begin -->
                        <div class="uk-width-1-2 uk-margin-medium-bottom">
                            <a href="http://drive.google.com/uc?export=view&id={{ $image->url }}"  data-lightbox="image-1">
                                <img src="https://drive.google.com/thumbnail?authuser=0&sz=w200&id={{ $image->url }}" alt=""/>
                            </a>
                        </div>
                        <!-- image sample end -->
                        @endforeach
                    </div>
                    <!-- property images end -->

                    <!-- property location begin -->
                    <h4 class="full_width_in_card heading_c">Location</h4>
                    <div id="map" style="width:100%; height:450px;"></div>
                    <!-- property location end -->




                </div>
            </div>
            <div class="uk-width-large-5-10">
                <div class="md-card user_content">
                    <h4 class="full_width_in_card heading_c">Properties</h4>
                    <div class="md-card-content">
                        <div class="uk-grid  uk-grid-medium">
                            <div class="uk-width-large-1-2">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">property type</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getUnitType($property->unit_type) }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Purpose</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getPurpose($property->purpose) }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Neighbour</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getNeighbour($property->Neighbour) }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Compound</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        {{ $property->isCompound($property->is_compound) }}
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Features</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        @foreach($property->getFeatures($property->id) as $feature)
                                        <span class="uk-badge uk-badge-primary">{{ $feature }}</span>
                                        @endforeach
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Objective</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getObjective($property->objective_id) }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">finishing</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getFinishing($property->finishing) }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="uk-width-large-1-2">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Price</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->price }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Area</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->area }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Rooms</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->rooms_no }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Bathrooms</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->toilets_no }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Public Services</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->distance_facilites }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Payment type</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{ $property->getPaymentType($property->payment_style) }}</span>
                                    </div>
                                </div>
                                <hr class="uk-grid-divider">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Notes</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-small">{{ $property->description }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="md-fab-wrapper" onclick="back()">
            <a class="md-fab md-fab-warning">
                <i class="material-icons"></i>
            </a>
        </div>

    </div>
    <!-- properties container end -->

</div>
<!-- google web fonts -->
<script>
WebFontConfig = {
    google: {
        families: [
            'Source+Code+Pro:400,700:latin',
            'Roboto:400,300,500,700,400italic:latin'
        ]
    }
};
(function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
</script>

<!-- common functions -->
<script src="../assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="../assets/js/uikit_custom.min.js"></script>
<!-- altair common functions/helpers -->
<script src="../assets/js/altair_admin_common.min.js"></script>

<!-- page specific plugins -->
<!-- ionrangeslider -->
<script src="../bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<!-- htmleditor (codeMirror) -->
<script src="../assets/js/uikit_htmleditor_custom.min.js"></script>
<!-- inputmask-->
<script src="../bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
<script src="{{ URL::asset('assets/js/lightbox2-master/src/js/lightbox.js') }}"></script>
<!--  forms advanced functions -->
<script src="../assets/js/pages/forms_advanced.min.js"></script>

<script>
$(function() {
    // enable hires images
    altair_helpers.retina_images();
    // fastClick (touch devices)
    if(Modernizr.touch) {
        FastClick.attach(document.body);
    }
});
</script>
<script type="text/javascript">
var marker;

    console.log({{ $property->longitude }} );
function initMap(){
    var lng = parseInt('{{ $property->longitude }}') ? parseInt('{{ $property->longitude }}') : 31.351973144531257;
    var lat = parseInt('{{ $property->latitude }}') ? parseInt('{{ $property->latitude }}') : 29.80217727185459;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: lat , lng: lng }
    });
    marker = new google.maps.Marker({
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: {lat: lat , lng: lng }
    });
}

</script>
<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZU-S9mUwRSmHBdbNiB9-3wITNXciVvcU&signed_in=true&callback=initMap"></script>


<script type="text/javascript" src="../js/general.js"></script>
<script type="text/javascript" src="../js/properties.js"></script>
</body>
</html>
