@if ($paginator->getLastPage() > 1)
 
    <ul class="uk-pagination">
    <li class="{{ ($paginator->getCurrentPage() == 1) ? ' uk-disabled' : '' }}"><a onclick="getPage(this , 1)" ><i class="uk-icon-angle-double-left"></i></a></li>
    @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
    <li class="{{ ($paginator->getCurrentPage() == $i) ? ' uk-active' : '' }}"><a onclick="getPage(this , '{{ $i }}')">{{ $i }}</a></li>
    @endfor
    <li class="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ? ' uk-disabled' : '' }}"><a onclick="getPage(this , {{$paginator->getLastPage() }})" ><i class="uk-icon-angle-double-right"></i></a></li>
    </ul>
 
@endif
