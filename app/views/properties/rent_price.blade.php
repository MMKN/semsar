<select readonly class="search_attr sltz uk-width-mini-2-3" id="price_from" name="price_from" onchange="updatePrices(this)" required data-md-selectize>
      <option value="" disabled selected>{{trans('main.Price_from')}}</option>
      <option value="700">700</option>
      <option value="1000">1000</option>
      <option value="1500">1500</option>
      <option value="2000">2000</option>
      <option value="3000">3000</option>
      <option value="5000">5000</option>
      <option value="7000">7000</option>
      <option value="10000">10000</option>
      <option value="15000">15000</option>
</select>