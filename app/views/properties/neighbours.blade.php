<select id="filter_neighbours" class="search_attr uk-width-mini-2-3" name="neighbour[]" data-md-selectize>
    <option value="" disabled selected>{{trans('main.District')}}</option>
    @foreach($neighbours as $neighbour)
        <option value="{{ $neighbour->id }}"> {{ $neighbour->name }} </option>
    @endforeach
</select>
