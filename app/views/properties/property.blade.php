<!-- property head begin -->
<div class=" uk-width-10-10 uk-container-center user_heading">

    <div class="user_heading_avatar" style="margin-top:13px">
        <i class="material-icons md-48 md-light uk-text-middle">&#xE88A;</i>
    </div>
    <div class="user_heading_content">
        <h2 class="heading_b uk-margin-bottom uk-float-left "><span class="uk-text-truncate">{{ $property->getUnitType($property->unit_type) }}</span><span class="sub-heading">{{ $property->key }}</span></h2>
        <ul class="user_stats uk-float-right">
            <!-- <li>
                <a onclick="add_request({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">call</i> Request a Call</a>
            </li> -->

            @if(!$f)
            <li>
                <a onclick="add_remove_favourite({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-middle">star</i> <span id="favourite_text"> {{trans('main.Add_to_fav')}} </span></a>
            </li>
            @else
            <li>
                <a onclick="add_remove_favourite({{ $property->id }})" class="md-btn"> <i class="material-icons uk-text-primary uk-text-danger uk-text-middle">star</i> <span id="favourite_text"> {{trans('main.remove_fav')}}</span></a>
            </li>
            @endif
        </ul>
    </div>
</div>
<!-- property head end -->



<div class="uk-grid">
    <div class="uk-width-large-5-10">
        <div class="md-card user_content">

            <!-- property images begin -->
            <h4 class="full_width_in_card heading_c uk-text-center">{{trans('main.Images')}}</h4>
            <div class="uk-grid" >
                @foreach($property->getImages($property->id) as $image)
                <!-- image sample begin -->
                <div class="uk-width-1-2 uk-width-mini-1-2 uk-margin-medium-bottom">
                    <a href="http://drive.google.com/uc?export=view&id={{ $image->url }}"  data-lightbox="image-1">
                        <img src="https://drive.google.com/thumbnail?authuser=0&sz=w400&id={{ $image->url }}" alt=""/>
                    </a>
                </div>
                <!-- image sample end -->
                @endforeach
            </div>
            <!-- property images end -->

            @if($property->show_map)
                <!-- property location begin -->
                <h4 class="full_width_in_card heading_c uk-text-center">{{trans('main.Location')}}</h4>
                <div id="map" style="width:100%; height:450px;"></div>
                <!-- property location end -->
            @endif


        </div>
    </div>
    <div class="uk-width-large-5-10">
        <div class="md-card user_content">
            <h4 class="full_width_in_card heading_c uk-text-center">{{trans('main.Property_preferences')}}</h4>
            <div class="md-card-content">
                <div class="uk-grid  uk-grid-medium uk-text-center">
                    <div class="uk-width-large-1-2">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Property_type')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getUnitType($property->unit_type) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Purpose')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getPurpose($property->purpose) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Neighbour')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getNeighbour($property->neighbour) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Floor')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getFloor($property->floor)->value }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Compound')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                {{ $property->isCompound($property->is_compound) }}
                            </div>
                        </div>
                        <!-- <hr class="uk-grid-divider"> -->
                        <div class="uk-grid uk-grid-small uk-hidden">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Features')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                @foreach($property->getFeatures($property->id) as $feature)
                                <span class="uk-badge uk-badge-primary">{{ $feature }}</span>
                                @endforeach
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Objective')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getObjective($property->objective_id) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Finishing')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getFinishing($property->finishing) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">


                    </div>
                    <div class="uk-width-large-1-2">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Price')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->price }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Area')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->area }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Rooms_No')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->rooms_no }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Toilets_No')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->toilets_no }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Distance_Facilites')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->distance_facilites }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Payment_style')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-middle">{{ $property->getPaymentType($property->payment_style) }}</span>
                            </div>
                        </div>
                        <hr class="uk-grid-divider">
                        <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3">
                                <span class="uk-text-muted uk-text-small">{{trans('main.Description')}}</span>
                            </div>
                            <div class="uk-width-large-2-3">
                                <span class="uk-text-large uk-text-small">{{ $property->description }}</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="md-fab-wrapper" onclick="back()">
    <a class="md-fab md-fab-warning">
       <!--  <i class="material-icons"></i> -->
        <i class="uk-text-small">{{trans('main.back')}}</i>
    </a>
</div>

<script type="text/javascript">
var marker;

    console.log({{ $property->longitude }} );
function initMap(){
    var lng = parseInt('{{ $property->longitude }}') ? parseInt('{{ $property->longitude }}') : 31.351973144531257;
    var lat = parseInt('{{ $property->latitude }}') ? parseInt('{{ $property->latitude }}') : 29.80217727185459;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: lat , lng: lng }
    });
    marker = new google.maps.Marker({
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: {lat: lat , lng: lng }
    });
}

</script>
<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZU-S9mUwRSmHBdbNiB9-3wITNXciVvcU&signed_in=true&callback=initMap"></script>
