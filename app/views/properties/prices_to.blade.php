<select class="search_attr sltz uk-width-mini-2-3" id="price_to" name="price_to" required data-md-selectize>
      <option value="" selected>{{trans('main.Price_to')}}</option>
      @foreach($prices as $price)
      <option value="{{$price}}">{{$price}}</option>
      @endforeach
</select>