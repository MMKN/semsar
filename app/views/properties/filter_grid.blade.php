<!-- Filter grid start  -->

<!-- Exact filter start -->
<div class="uk-width-1-1 uk-container-center">
    <h3 style="background-color: #fff; padding: 0.6em; text-align:center;">{{trans('main.Exact_Properties')}}</h3>
    <div class="full_width_in_card heading_c" style="float:left;">
    </div>
</div>
<div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
    @if(count($properties))
        @foreach($properties as $property)
        <!-- property start -->
        <div style="margin-bottom:27px;" onclick="viewProperty({{$property->id}})">
            <div class="md-card md-card-hover md-card-overlay get_view_box" style="cursor:pointer;" >
                <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($property->id) }}')">

                    <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                        {{ $property->getUnitType($property->unit_type)}}
                    </h2>
                    <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $property->price }} LE</div>
                </div>
                <div class="md-card-head-menu uk-hidden">
                    <i class="md-icon material-icons md-36 md-light" >stars</i>
                </div>
                <div class="md-card-content">
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{ $property->getCity($property->city) }}</span>
                                <span class="uk-text-small uk-text-muted">{{ $property->getNeighbour($property->neighbour) }}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- property end -->
        @endforeach
    @else
        <div class="uk-width-medium-1-1 uk-margin-medium-bottom" style="margin:0 auto;">
            <i class="material-icons md-48 uk-text-warning">youtube_searched_for</i>
        </div>
    @endif
</div>
<!-- Exact filter end -->



<!-- Suggested filter start -->
<div class="uk-width-1-1 uk-container-center">
    <h3 style="background-color: #fff; padding: 0.6em; text-align:center;">{{trans('main.Suggested_Properties')}}</h3>
    <div class="full_width_in_card heading_c" style="float:left;">
    </div>
</div>
<div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
    @if(count($suggestions))
        @foreach($suggestions as $suggestion)
        <!-- property start -->
        <div style="margin-bottom:27px;" onclick="viewProperty({{$suggestion->id}})">
            <div class="md-card md-card-hover md-card-overlay get_view_box" style="cursor:pointer;" >
                <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($suggestion->id) }}')">

                    <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                        {{ $suggestion->getUnitType($suggestion->unit_type)}}
                    </h2>
                    <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $suggestion->price }} LE</div>
                </div>
                <div class="md-card-head-menu uk-hidden">
                    <i class="md-icon material-icons md-36 md-light">stars</i>
                </div>
                <div class="md-card-content">
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{ $suggestion->getCity($suggestion->city) }}</span>
                                <span class="uk-text-small uk-text-muted">{{ $suggestion->getNeighbour($suggestion->neighbour) }}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- property end -->
        @endforeach
    @else
        <div class="uk-width-medium-1-1 uk-margin-medium-bottom" style="margin:0 auto;">
            <i class="material-icons md-48 uk-text-warning">youtube_searched_for</i>
        </div>
    @endif
</div>
<!-- Suggested filter end -->

<!-- Filter grid end  -->
<div class="uk-width-1-1 uk-container-center">
    <h3 style="background-color: #fff; padding: 0.6em; text-align:center;">{{trans('main.Price_Properties')}}</h3>
    <div class="full_width_in_card heading_c" style="float:left;">
    </div>
</div>
<div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
    @if(count($price_properties))
        @foreach($price_properties as $price_property)
        <!-- property start -->
        <div style="margin-bottom:27px;" onclick="viewProperty({{$price_property->id}})">
            <div class="md-card md-card-hover md-card-overlay get_view_box" style="cursor:pointer;" >
                <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $price_property->getMainImage($price_property->id) }}')">

                    <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                        {{ $price_property->getUnitType($price_property->unit_type)}}
                    </h2>
                    <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $price_property->price }} LE</div>
                </div>
                <div class="md-card-head-menu uk-hidden">
                    <i class="md-icon material-icons md-36 md-light">stars</i>
                </div>
                <div class="md-card-content">
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">{{ $price_property->getCity($price_property->city) }}</span>
                                <span class="uk-text-small uk-text-muted">{{ $price_property->getNeighbour($price_property->neighbour) }}</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- property end -->
        @endforeach
    @else
        <div class="uk-width-medium-1-1 uk-margin-medium-bottom" style="margin:0 auto;">
            <i class="material-icons md-48 uk-text-warning">youtube_searched_for</i>
        </div>
    @endif
</div>