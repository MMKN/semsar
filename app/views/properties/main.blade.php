<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">
    <title>{{trans('main.project_name')}}</title>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">
    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">
    <link rel="stylesheet" href="{{ URL::asset('assets/js/lightbox2-master/src/css/lightbox.css') }}" media="all">
    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->
</head>
<body class="top_menu">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <!-- <div class="main_logo_top">
                    <a href="index.html"><img src="assets/img/white.png" alt=""     height="15" width="71"/></a>
                </div > -->
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->
                <!--
                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons uk-margin-large-right md-24">info</i><i class="material-icons md-24">call</i> 01123777760</a>
                        <div class="uk-dropdown uk-dropdown-width-3">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-1-2">
                                    <div class="uk-grid uk-container-center uk-grid-width-medium-1-2 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                        
                                        <a href="http://minionz.net" target="_blank">
                                            <span class="uk-text-muted uk-display-block uk-margin-bottom">Developed by</span>
                                            <img src="assets/img/favicon.png" alt=""     height="15" width="71"/>
                                            <span class="uk-text-muted uk-display-block uk-margin">Minionz</span>
                                        </a>
                                        
                                    </div>
                                </div>
                                <div class="uk-width-1-2">
                                    <ul class="uk-nav uk-nav-dropdown uk-panel">
                                        <li class="uk-nav-header">وسائل الإتصال</li>
                                        <li><a >01123777760</a></li>
                                        <li><a >01149934994</a></li>
                                        <li><a >01149935993</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->
                
                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="tel:+2-011-237-77760" class="top_menu_toggle"><i class="material-icons uk-margin-large-right md-24">info</i><i class="material-icons md-24">call</i> 01123777760</a>
                        <div class="uk-dropdown uk-dropdown-width-1">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-1-1">
                                    <div class="uk-grid  uk-grid-width-medium-1-1 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                        <a href="http://minionz.net" target="_blank">
                                            <span class="uk-text-muted uk-display-block uk-margin-bottom">Developed by</span>
                                            <img src="assets/img/favicon.png" alt=""     height="15" width="71"/>
                                            <span class="uk-text-muted uk-display-block uk-margin">Minionz</span>
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                @if(Session::get('id'))
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-warning" href="{{ URL::route('profile.main') }}" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> {{trans('main.my_account')}}</a>
                </div>
                @else
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-primary" href="fb" id="fb_button" style="margin-top: 6px;"><i class="uk-icon-facebook uk-icon-small" style="color:#fff"></i> {{trans('main.sing_in')}}</a>
                </div>
                @endif
                <!-- <div class="uk-navbar-flip" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>
                    <div class="uk-dropdown uk-dropdown-small uk-text-center">
                        <ul class="uk-nav js-uk-prevent">
                            <li><a href="page_user_profile.html">My profile</a></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                </div> -->
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <!-- notification begin -->
                        @if(count($notifications))
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon">
                                <i class="material-icons md-24 md-light">&#xE7F4;</i>
                                <span class="uk-badge" style="background:#f00">
                                    {{ count($unread_notifications) }}
                                </span>
                            </a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    </ul>
                                    <ul class="uk-margin">
                                        @if(count($notifications))
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                @foreach($notifications->take(5) as $notification)
                                                <li style="cursor:pointer">
                                                    <div class="md-list-addon-element">
                                                        <i class="material-icons md-36 uk-text-warning">new_releases</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">
                                                            <a href="{{ URL::route('property' , $notification->property_id) }}" target=_blank>
                                                                We have found you new matching property
                                                            </a>
                                                        </span>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                            <!-- <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="#" class="md-btn md-btn-primary js-uk-prevent">Check it now</a>
                                            </div> -->
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            </form>
        </div>
    </header>
    <!-- main header end -->
        <div id="page_content">
            <!-- filter start -->
            <div id="page_heading" >
                <form action="{{ URL::route('properties.filter') }}" id="filter_form">
                    <div class="uk-grid uk-text-center uk-container-center" data-uk-grid-margin>
                        <h1 class="uk-width-medium-2-10 uk-margin-medium-left  uk-text-center uk-margin-bottom" style="margin-top:12px;"><i class="md-icon material-icons uk-margin-right">home</i>{{trans('main.properties')}}</h1>
                        <div class="uk-width-medium-2-10">
                            <div class="uk-form-row parsley-row uk-margin-small-top " style="padding-top:5px;">
                                <span class="icheck-inline uk-margin-small-top">
                                    <input class="search_attr_change ichk add-sale" id="sale" type="radio" name="purpose" value="0" checked data-md-icheck required />
                                    <label for="sale" class="inline-label">{{trans('main.Sale')}}</label>
                                </span>
                                <span class="icheck-inline">
                                    <input class="search_attr_change ichk" id="rent" type="radio" name="purpose" value="1" data-md-icheck />
                                    <label for="rent" class="inline-label">{{trans('main.Rent')}}</label>
                                </span>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10">
                            <div class="uk-margin-small-top">
                                <select class="search_attr" id="filter_unit_type" name="unit_type[]" data-md-selectize  >
                                    <option value="" disabled selected>{{trans('main.Property_type')}}</option>
                                    @foreach($unit_types as $unit_type)
                                    <option value="{{ $unit_type->id }}">{{ $unit_type->value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-10 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select name="city" class="search_attr" required data-md-selectize id="filter_city">
                                    <option value="" disabled selected>{{trans('main.City')}}</option>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10 uk-text-center" id="neighbour_search">
                            <div class="parsley-row  uk-margin-small-top" id="filter_neighbours_container">
                                <select id="filter_neighbours" disabled class="search_attr" name="neighbour[]" data-md-selectize  >
                                    <option value="" disabled selected>{{trans('main.District')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-10 ">
                            <div class="uk-input-group uk-container-center" style="margin-top:13px;">
                                <input type="checkbox" id="compound" name="compound" data-switchery data-switchery-color="#ffa000" />
                                <label for="compound" class="inline-label">{{trans('main.Compound')}}</label>
                            </div>
                        </div>
                        <!-- <div class="uk-width-medium-1-10">
                            <label for="masked_currency">{{trans('main.Price_from')}}</label>
                            <input name="price_from" class="md-input masked_input" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                        </div>
                        <div class="uk-width-medium-1-10">
                            <label for="masked_currency">{{trans('main.Price_to')}}</label>
                            <input name="price_to" class="md-input masked_input" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                        </div> -->
                        <div class="uk-width-medium-1-10 uk-hidden">
                            <label>{{trans('main.Area')}}</label>
                            <input class="md-input masked_input search_attr" name="area" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': '', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
                        </div>
                        <div class="uk-width-medium-2-10 ">
                            <div class="parsley-row uk-margin-small-top">
                                <select class="search_attr" id="floor_search" name="floor[]" data-md-selectize  >
                                    <option value="" disabled selected>{{trans('main.Floor')}}</option>
                                    @foreach($floors as $floor)
                                    <option value="{{ $floor->id }}">{{ $floor->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-10">
                            <div class="parsley-row  uk-margin-small-top">
                                <select class="search_attr" id="objective_search" name="objective" required data-md-selectize>
                                    <option value="" disabled selected>{{trans('main.Objective')}}</option>
                                    @foreach($objectives as $objective)
                                    <option value="{{ $objective->id }}">{{ $objective->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10 payment_hide">
                            <div class="parsley-row  uk-margin-small-top">
                                <select class="search_attr" id="payment_search" name="payment_style[]" data-md-selectize >
                                    <option value="" disabled selected>{{trans('main.Payment_style')}}</option>
                                    @foreach($payment_types as $payment_type)
                                    <option value="{{ $payment_type->id }}">{{ $payment_type->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10 ">
                            <div class="parsley-row  uk-margin-small-top">
                                <select class="search_attr" id="finishing_search" name="finishing[]" data-md-selectize >
                                    <option value="" disabled selected>{{trans('main.Finishing')}}</option>
                                    @foreach($finishings as $finishing)
                                    <option value="{{ $finishing->id }}">{{ $finishing->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-10">
                            <!-- <label>{{trans('main.Price_from')}}</label> -->
                            <div class="parsley-row  uk-margin-small-top" id="price_from_selector">
                                <select class="search_attr" id="price_from" onchange="updatePrices(this)" name="price_from" required data-md-selectize>
                                    <option value="" disabled selected>{{trans('main.Price_from')}}</option>
                                    <option value="60000">60000</option>
                                    <option value="100000">100000</option>
                                    <option value="125000">125000</option>
                                    <option value="150000">150000</option>
                                    <option value="175000">175000</option>
                                    <option value="200000">200000</option>
                                    <option value="250000">250000</option>
                                    <option value="300000">300000</option>
                                    <option value="350000">350000</option>
                                    <option value="400000">400000</option>
                                    <option value="450000">450000</option>
                                    <option value="500000">500000</option>
                                    <option value="600000">600000</option>
                                    <option value="700000">700000</option>
                                    <option value="800000">800000</option>
                                    <option value="900000">900000</option>
                                    <option value="1000000">1000000</option>
                                    <option value="1200000">1200000</option>
                                    <option value="1400000">1400000</option>
                                    <option value="1600000">1600000</option>
                                    <option value="1800000">1800000</option>
                                    <option value="2000000">2000000</option>
                                    <option value="2300000">2300000</option>
                                    <option value="2600000">2600000</option>
                                    <option value="3000000">3000000</option>
                                    <option value="3500000">3500000</option>
                                    <option value="4000000">4000000</option>
                                    <option value="5000000">5000000</option>
                                    <option value="6000000">6000000</option>
                                    <option value="7500000">7500000</option>
                                    <option value="10000000">10000000</option>
                                    <option value="15000000">15000000</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-10">
                            <!-- <label>{{trans('main.Price_from')}}</label> -->
                            <div class="parsley-row  uk-margin-small-top" id="price_to_selector">
                                <select class="search_attr" id="price_to" name="price_to" required data-md-selectize>
                                    <option value="" disabled selected>{{trans('main.Price_to')}}</option>
                                    <option value="100000">100000</option>
                                    <option value="125000">125000</option>
                                    <option value="150000">150000</option>
                                    <option value="175000">175000</option>
                                    <option value="200000">200000</option>
                                    <option value="250000">250000</option>
                                    <option value="300000">300000</option>
                                    <option value="350000">350000</option>
                                    <option value="400000">400000</option>
                                    <option value="450000">450000</option>
                                    <option value="500000">500000</option>
                                    <option value="600000">600000</option>
                                    <option value="700000">700000</option>
                                    <option value="800000">800000</option>
                                    <option value="900000">900000</option>
                                    <option value="1000000">1000000</option>
                                    <option value="1200000">1200000</option>
                                    <option value="1400000">1400000</option>
                                    <option value="1600000">1600000</option>
                                    <option value="1800000">1800000</option>
                                    <option value="2000000">2000000</option>
                                    <option value="2300000">2300000</option>
                                    <option value="2600000">2600000</option>
                                    <option value="3000000">3000000</option>
                                    <option value="3500000">3500000</option>
                                    <option value="4000000">4000000</option>
                                    <option value="5000000">5000000</option>
                                    <option value="6000000">6000000</option>
                                    <option value="7500000">7500000</option>
                                    <option value="10000000">10000000</option>
                                    <option value="15000000">15000000</option>
                                </select>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="uk-grid">
                        <div class="uk-text-center uk-container-center">
                            
                            <!-- <i class="material-icons uk-float-left uk-margin-left  md-36 uk-text-primary" data-uk-tooltip="{pos:'left'}" title="{{trans('main.Search')}}" id="filter_search_button" style="cursor:pointer;">search</i> -->
                            <a class="md-btn md-btn-light"  id="filter_search_button" style="margin-top: 6px;">{{trans('main.Search')}}</a>
                            <a class="md-btn md-btn-light uk-text-danger"  id="filter_reset_button" style="margin-top: 6px;">{{trans('main.Reset')}}</a>
                            <!-- <i class="material-icons  uk-margin-left md-36 uk-text-danger" data-uk-tooltip="{pos:'right'}" title="{{trans('main.Reset')}}" id="filter_reset_button" style="cursor:pointer;">clear</i> -->
                            <!-- <i class="material-icons  uk-margin-left md-36 uk-text-success" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.save_preferences')}}" onclick="UIkit.modal.confirm('Your Prefrences Wil be Overwritten. Are You Sure ?', save_filter);" style="cursor:pointer;">save</i> -->
                        </div>
                    </div>
                </form>
            </div>
            <!-- filter end -->
            <!-- properties container start -->
            <div id="page_content_inner" class="hierarchical_show" onscroll="scrolldetect()">
                <!-- properties grid start  -->
                <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                    @foreach($properties as $property)
                    <!-- property start -->
                    <div style="margin-bottom:27px;"  >
                        <div class="md-card md-card-hover md-card-overlay get_view_box" onclick="viewProperty({{$property->id}})" style="cursor:pointer;" >
                            <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($property->id) }}')">
                                <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                                {{ $property->getUnitType($property->unit_type)}}
                                </h2>
                                <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $property->price }} LE</div>
                            </div>
                            <div class="md-card-head-menu uk-hidden">
                                <i class="md-icon material-icons md-36 md-light" >stars</i>
                            </div>
                            <div class="md-card-content">
                                <ul class="md-list md-list-addon">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">{{ $property->getCity($property->city) }}</span>
                                            <span class="uk-text-small uk-text-muted">{{ $property->getNeighbour($property->neighbour) }}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- property end -->
                    @endforeach
                </div>
                <!-- properties grid end  -->
                <!-- pagination start -->
                <div class="uk-grid">
                    <div class="uk-width-medium-1-1 pager">
                        {{$properties->links()}}
                    </div>
                </div>
                <!-- pagination end -->
            </div>
            <!-- properties container end -->
            <div id="property">
                
            </div>
        </div>
        <div class="uk-modal" id="mobile_modal">
            <div class="uk-modal-dialog">
                <form class="uk-form-stacked">
                    <div class="uk-margin-medium-bottom">
                        <label for="task_title">You Must provide mobile number to request a call</label>
                        <input type="text" class="md-input" id="user_mobile" name="user_mobile"/>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="button" onclick="submitMobile()" class="md-btn md-btn-flat md-btn-flat-primary" id="save_mobile">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- google web fonts -->
        <script>
        WebFontConfig = {
        google: {
        families: [
        'Source+Code+Pro:400,700:latin',
        'Roboto:400,300,500,700,400italic:latin'
        ]
        }
        };
        (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
        })();
        </script>
        <script src="assets/js/common.min.js"></script>
        <script src="assets/js/uikit_custom.min.js"></script>
        <script src="assets/js/altair_admin_common.min.js"></script>
        <!-- <script src="{{ URL::asset('bower_components/uikit/js/uikit.min.js') }}"></script> -->
        <script src="{{ URL::asset('bower_components/uikit/js/components/lightbox.min.js') }}"></script>
        <script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
        <script src="assets/js/uikit_htmleditor_custom.min.js"></script>
        <script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>
        <script src="{{ URL::asset('assets/js/lightbox2-master/src/js/lightbox.js') }}"></script>
        <!--  forms advanced functions -->
        <script src="assets/js/pages/forms_advanced.min.js"></script>
        <script>
        var last_scroll = 0;
        var base_url = '{{ URL::route("properties.main") }}';
        var homepage = 0;
        // $(window).scroll(function() {
        //     if (!homepage) {
        //         var direction='none';
        //         var current_scroll=$(this).scrollTop();
        //        if(current_scroll < last_scroll ){
        //            if(current_scroll < 760)
        //           $("#page_heading").slideDown("280");
        //            direction='up';
        //        }
        //        else if( current_scroll > 760 ) {
        //           $("#page_heading").slideUp("280");
        //           console.log(current_scroll);
        //            direction = 'down';
        //        }
        //         last_scroll = current_scroll;
        //     }
        // });
        </script>
        <script>
        $(function() {
        $(".selectize-input input").attr('readonly','readonly');
        // enable hires images
        altair_helpers.retina_images();
        // fastClick (touch devices)
        if(Modernizr.touch) {
        FastClick.attach(document.body);
        }
        });
        </script>>
        <script type="text/javascript" src="js/general.js"></script>
        <script type="text/javascript" src="js/properties.js"></script>
    </body>
</html>