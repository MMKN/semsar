<!-- properties grid start  -->
<div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
    @foreach($properties as $property)
    <!-- property start -->
    <div style="margin-bottom:27px;" id="property_card" >
        <div class="md-card md-card-hover md-card-overlay get_view_box" onclick="viewProperty({{$property->id}})" style="cursor:pointer;" >
            <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($property->id) }}')">

                <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                    {{ $property->getUnitType($property->unit_type)}}
                </h2>
                <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $property->price }} LE</div>
            </div>
            <div class="md-card-head-menu uk-hidden">
                <i class="md-icon material-icons md-36 md-light" >stars</i>
            </div>
            <div class="md-card-content">
                <ul class="md-list md-list-addon">
                    <li>
                        <div class="md-list-addon-element">
                            <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                        </div>
                        <div class="md-list-content">
                            <span class="md-list-heading">{{ $property->getCity($property->city) }}</span>
                            <span class="uk-text-small uk-text-muted">{{ $property->getNeighbour($property->neighbour) }}</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- property end -->
    @endforeach
</div>
<!-- properties grid end  -->

<!-- pagination start -->
<div class="uk-grid">
    <div class="uk-width-medium-1-1 pager">
        {{$properties->links()}}
    </div>
</div>
<!-- pagination end -->
