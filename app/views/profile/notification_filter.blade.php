<div class="md-card-list">
    <div class="md-card-list-header heading_list">Today</div>
    <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
    <ul class="hierarchical_slide">
    @foreach($notifications as $notification)
        <li>
            <div class="md-card-list-item-menu" >
                <a href="#" class="md-icon material-icons">done</a>
            </div>
            <span class="md-card-list-item-date ">12 Nov</span>
            <span class="md-card-list-item-date uk-badge uk-badge-danger uk-margin-right" style="color:#fff">Open</span>
            <span class="md-card-list-item-date uk-badge uk-badge-success uk-margin-right" style="color:#fff">Done</span>

            <div class="md-card-list-item-select">
                <input type="checkbox" data-md-icheck />
            </div>
            <div class="md-card-list-item-avatar-wrapper">
                <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
            </div>
            <div class="md-card-list-item-sender">
                <span>Customer Name/ Property Name</span>
            </div>
            <div class="md-card-list-item-subject">
                <div class="md-card-list-item-sender-small">
                    <span>Customer Name/ Property Name</span>
                </div>
                <span>Requested a Call / New Notice.</span>
            </div>
            <div class="md-card-list-item-content-wrapper">
                <!-- here goes description if exist  -->
                <div class="md-card-list-item-content">
                    Earum maxime ratione itaque et consequatur sed laborum sed mollitia architecto laudantium ipsa porro molestiae accusamus numquam corrupti voluptatum quia eum blanditiis et molestiae illum explicabo suscipit autem non porro voluptatem molestiae atque est molestias hic officiis quisquam velit quis sint et consequuntur et consequuntur sit dolores non necessi
                </div>
                <div class="md-card-list-item-content">
                    here is a timeline with any updates regarding the notification
                    <div class="timeline">
                        <div class="timeline_item">
                            <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                            <div class="timeline_date">
                                21 <span>Sep</span>
                            </div>
                            <div class="timeline_content">
                                Something about the customer goes here something about the customer goes here!


                            </div>
                        </div>

                    </div>                         
                </div>
                <form class="md-card-list-item-reply uk-container-center ">
                    <label for="mailbox_reply_7254">Add update</label>
                    <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
                    <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                </form>
            </div>
        </li>
        @endforeach
    </ul>
</div>