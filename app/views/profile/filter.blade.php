<form action="{{ URL::route('properties.filter') }}" id="filter_form">
    <div class="uk-grid" data-uk-grid-margin>
        <h1 class="uk-width-10-10 uk-text-center full_width_in_card heading_c uk-margin-medium-left uk-margin-large-bottom uk-margin-bottom" style="margin-top:12px;"><i class="md-icon material-icons  uk-margin-right">person</i>{{trans('main.personal_preferences')}}</h1>

        <div>
            <div class="uk-form-row parsley-row uk-margin-small-top " style="padding-top:5px;">
                <span class="icheck-inline uk-margin-small-top">
                    <input class="search_attr_change ichk add-sale" {{$user->purpose == 0 ? 'checked' : ''}} id="sale" type="radio" name="purpose" value="0" checked data-md-icheck required />
                    <label for="sale" class="inline-label">{{trans('main.Sale')}}</label>
                </span>
                <span class="icheck-inline">
                    <input class="search_attr_change ichk" {{$user->purpose == 1 ? 'checked' : ''}} id="rent" type="radio" name="purpose" value="1" data-md-icheck />
                    <label for="rent" class="inline-label">{{trans('main.Rent')}}</label>
                </span>
            </div>
        </div>

        <div class="uk-width-medium-2-10">
            <div class="uk-margin-small-top">
                <select class="search_attr" id="filter_unit_type" name="unit_type[]" data-md-selectize  data-md-selectize-bottom>
                    <option value="" disabled >{{trans('main.Property_type')}}</option>
                    @foreach($unit_types as $unit_type)
                    <option value="{{ $unit_type->id }}" {{in_array($unit_type->id , explode("|" , $user->unit_type)) ?"selected" : ""}}>{{ $unit_type->value}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="uk-width-medium-1-10 ">
            <div class="parsley-row  uk-margin-small-top">
                <select name="city" class="search_attr" required data-md-selectize id="filter_city" onchange="getListChange('#filter_neighbours_container' , this.value)">
                    <option value="" disabled selected>{{trans('main.City')}}</option>
                    @foreach($cities as $city)
                    <option value="{{ $city->id }}" {{$user->city == $city->id? "selected" : ""}}>{{ $city->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="uk-width-medium-2-10 uk-text-center" id="neighbour_search">
            <div class="parsley-row  uk-margin-small-top" id="filter_neighbours_container">
                <select id="filter_neighbours" class="search_attr" name="neighbour[]" data-md-selectize  data-md-selectize-bottom>
                    <option value="" disabled selected>{{trans('main.District')}}</option>
                    @foreach($selected_neighbours as $neighbour)
                    <option value="{{ $neighbour->id }}" {{in_array($neighbour->id , explode("|" , $user->neighbour))?"selected" : ""}}>{{ $neighbour->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="uk-width-medium-1-10">
            <label for="masked_currency">Price from</label>
            <input name="price_from" class="md-input masked_input" value="{{$user->price_from}}" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
        </div>
        <div class="uk-width-medium-1-10">
            <label for="masked_currency">Price to</label>
            <input name="price_to" class="md-input masked_input" value="{{$user->price_to}}" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
        </div>
        <div class="uk-width-medium-1-10">
            <label>area</label>
            <input class="md-input masked_input search_attr" value="{{$user->area}}" name="area" type="text" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 0, 'digitsOptional': false , 'placeholder': '0'" data-inputmask-showmaskonhover="false" />
        </div>


        <div class="uk-width-medium-2-10 ">
            <div class="parsley-row uk-margin-small-top">
                <select class="search_attr" id="floor_search" name="floor[]" data-md-selectize  data-md-selectize-bottom>
                    <option value="" disabled >{{trans('main.Floor')}}</option>
                    @foreach($floors as $floor)
                    <option value="{{ $floor->id }}" {{in_array($floor->id , explode("|" , $user->floor))?"selected" : ""}}>{{ $floor->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="uk-width-medium-1-10">
            <div class="parsley-row  uk-margin-small-top">
                <select class="search_attr" id="objective_search" name="objective" required data-md-selectize>
                    <option value="" disabled >{{trans('main.Objective')}}</option>
                    @foreach($objectives as $objective)
                    <option value="{{ $objective->id }}" {{in_array($objective->id , explode("|" , $user->objective))?"selected" : ""}}>{{ $objective->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="uk-width-medium-1-10">
            <div class="parsley-row  uk-margin-small-top">
                <select class="search_attr" id="payment_search" name="payment_style[]" data-md-selectize  data-md-selectize-bottom>
                    <option value="" disabled >{{trans('main.Payment_style')}}</option>
                    @foreach($payment_types as $payment_type)
                    <option value="{{ $payment_type->id }}" {{in_array($payment_type->id , explode("|" , $user->payment_type))?"selected" : ""}}>{{ $payment_type->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="uk-width-medium-2-10 ">
            <div class="parsley-row  uk-margin-small-top">
                <select class="search_attr" id="finishing_search" name="finishing[]" data-md-selectize  data-md-selectize-bottom>
                    <option value="" disabled >{{trans('main.Finishing')}}</option>
                    @foreach($finishings as $finishing)
                    <option value="{{ $finishing->id }}" {{in_array($finishing->id , explode("|" , $user->finishing))?"selected" : ""}}>{{ $finishing->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="">
            <div class="uk-input-group " style="margin-top:13px;">
                <input type="checkbox" name="compound" {{$user->is_compound == 1 ? 'checked' : ''}} data-switchery data-switchery-color="#ffa000" id="switch_demo_success" />
                <label for="switch_demo_success" class="inline-label">{{trans('main.Compound')}}</label>
            </div>
        </div>
        <div class="">
            <a class="md-btn md-btn-flat  id="filter_save_button" style="margin-top:8px" md-btn-flat-wave-light" onclick="save_filter()" href="#"><i class="material-icons">save</i> {{trans('main.save_preferences')}}</a>

            <!-- <i class="material-icons md-36 uk-text-primary" data-uk-tooltip="{pos:'bottom'}" title="{{trans('main.save_preferences')}}" title="" onclick="save_filter()" style="cursor:pointer;">save</i> -->
        </div>

    </div>
</form>
