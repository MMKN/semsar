<!-- user heading  -->
<div class="user_heading" data-uk-sticky="{ top: 0, media: 960 }">
    <div class="user_heading_avatar">
        <img src="http://graph.facebook.com/v2.5/{{ $user->id }}/picture?type=large" alt="user avatar"/>
    </div>
    <div class="user_heading_content" style="padding:0px 0px;">
        <h2 class="heading_b uk-margin-bottom uk-float-left"><span class="uk-text-truncate">{{ $user->name }}</span><span class="sub-heading"  style="margin-top: 5px;"><i class="material-icons md-24 md-light">smartphone</i> {{ $user->mobile }} </span><span class="sub-heading"  style="margin-top: 5px;"><i class="material-icons md-24 md-light">email</i> {{ $user->email }} </span></h2>
        <ul class="user_stats uk-float-right" style="margin-top: 30px;">
           <!--  <li>
                <a class="md-btn" onclick="add_request(0)"> <i class="material-icons uk-text-primary uk-text-middle">call</i> Request a Call</a>
            </li> -->
            <li>
                <a class="md-btn" href="#" onclick="viewPrefs()" id="view_preferences"> <i class="material-icons uk-text-primary uk-text-middle">edit</i>{{trans('main.view_edit_profile')}}</a>
            </li>
        </ul>
    </div>
</div>
<div class="uk-grid uk-container-center">
                    <!-- favourtie properties begin -->
                    <div class="md-card user_content uk-width-10-10">
                        <h4 class="full_width_in_card uk-text-center heading_c"><i class="material-icons md-24 uk-margin-right">star</i> {{trans('main.Favourite_Properties')}}</h4>
                        <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                            @foreach($favourites as $favourite)
                            <!-- property start -->
                            <div style="margin-bottom:27px;"  onclick="viewProperty({{$favourite->id}})">
                                <div class="md-card md-card-hover md-card-overlay get_view_box" style="cursor:pointer;" >
                                    <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $favourite->getMainImage($favourite->id) }}')">

                                        <h2 class="md-card-head-text" style="text-shadow: 2px 2px 5px #000; font-size:22px">
                                            {{ $favourite->getUnitType($favourite->unit_type)}}
                                        </h2>
                                        <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{ $favourite->price }} LE</div>
                                    </div>
                                    <div class="md-card-head-menu uk-hidden">
                                        <i class="md-icon material-icons md-36 md-light" >stars</i>
                                    </div>
                                    <div class="md-card-content">
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">{{ $favourite->getCity($favourite->city) }}</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $favourite->getNeighbour($favourite->neighbour) }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- property end -->
                            <!-- property sample begin -->

                            <!-- property sample end  -->
                            @endforeach
                        </div>
                    </div>


                    <!-- matching properties begin -->
                    <div class="md-card user_content uk-width-10-10">
                        <h4 class="full_width_in_card uk-text-center heading_c"><i class="material-icons md-24 uk-margin-right">thumb_up</i>  {{trans('main.best_match')}} </h4>
                        <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">

                            <!-- property sample begin -->
                            @foreach($properties as $property)
                            <div style="margin-bottom:27px;"  onclick="viewProperty({{$property->id}})">
                                <div class="md-card md-card-hover md-card-overlay get_view_box" id="property" style="cursor:pointer;" >
                                    <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $property->getMainImage($property->id) }}')">

                                        <h2 class="md-card-head-text" style="    text-shadow: 2px 2px 5px #000; font-size:22px">
                                            {{$property->getUnitType($property->unit_type)}}
                                        </h2>
                                            <!-- <div class="md-card-head-subtext">
                                            <span>500000&#36;</span>
                                        </div> -->
                                        <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{$property->price}} &nbsp; LE</div>
                                    </div>
                                    <div class="md-card-head-menu uk-hidden">
                                        <i class="md-icon material-icons md-36 md-light">stars</i>
                                    </div>
                                    <div class="md-card-content">
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">{{(new City())->getCity($property->city)}} </span>
                                                    <span class="uk-text-small uk-text-muted">{{(new Neighbour())->getNeighbour($property->neighbour)}}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>

                    <!-- matching properties begin -->
                    <div class="md-card user_content uk-width-10-10">
                        <h4 class="full_width_in_card uk-text-center heading_c"><i class="material-icons md-24 uk-margin-right">thumb_up</i>  {{trans('main.suggest_match')}} </h4>
                        <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">

                            <!-- property sample begin -->
                            @foreach($suggestions as $suggestion)
                            <div style="margin-bottom:27px;"  onclick="viewProperty({{$suggestion->id}})">
                                <div class="md-card md-card-hover md-card-overlay get_view_box" id="suggestion" style="cursor:pointer;" >
                                    <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $suggestion->getMainImage($suggestion->id) }}')">

                                        <h2 class="md-card-head-text" style="    text-shadow: 2px 2px 5px #000; font-size:22px">
                                            {{$suggestion->getUnitType($suggestion->unit_type)}}
                                        </h2>
                                            <!-- <div class="md-card-head-subtext">
                                            <span>500000&#36;</span>
                                        </div> -->
                                        <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{$suggestion->price}} &nbsp; LE</div>
                                    </div>
                                    <div class="md-card-head-menu uk-hidden">
                                        <i class="md-icon material-icons md-36 md-light">stars</i>
                                    </div>
                                    <div class="md-card-content">
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">{{(new City())->getCity($suggestion->city)}} </span>
                                                    <span class="uk-text-small uk-text-muted">{{(new Neighbour())->getNeighbour($suggestion->neighbour)}}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                    
                    <!-- matching properties begin -->
                    <!-- <div class="md-card user_content uk-width-10-10">
                        <h4 class="full_width_in_card uk-text-center heading_c"><i class="material-icons md-24 uk-margin-right">thumb_up</i>  {{trans('main.price_match')}} </h4>
                        <div id="view_box" class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">

                            @foreach($price_properties as $price_property)
                            <div style="margin-bottom:27px;"  onclick="viewProperty({{$price_property->id}})">
                                <div class="md-card md-card-hover md-card-overlay get_view_box" id="price_property" style="cursor:pointer;" >
                                    <div class="md-card-head head_background" style="box-shadow: rgba(0,0,0,0.1) 0px 40px 20px inset; background-image: url('{{ $price_property->getMainImage($price_property->id) }}')">

                                        <h2 class="md-card-head-text" style="    text-shadow: 2px 2px 5px #000; font-size:22px">
                                            {{$price_property->getUnitType($price_property->unit_type)}}
                                        </h2>
                                        <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-bottom-right uk-margin-bottom uk-margin-top" style="font-size:18px;">{{$price_property->price}} &nbsp; LE</div>
                                    </div>
                                    <div class="md-card-head-menu uk-hidden">
                                        <i class="md-icon material-icons md-36 md-light">stars</i>
                                    </div>
                                    <div class="md-card-content">
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="material-icons md-36 uk-text-primary ">&#xE0C8;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">{{(new City())->getCity($price_property->city)}} </span>
                                                    <span class="uk-text-small uk-text-muted">{{(new Neighbour())->getNeighbour($price_property->neighbour)}}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div> -->
                </div>

<!-- user Preferences begin -->
<div class="uk-width-large-5-10 uk-hidden">
    <div class="md-card user_content">
        <h4 class="full_width_in_card uk-text-center heading_c">Properties</h4>
        <div class="md-card-content">
            <div class="uk-grid  uk-grid-medium">
                <div class="uk-width-large-1-2">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">property type</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle"><a href="#">Villa</a></span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Purpose</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">sale</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">City</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">New Cairo</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Compound</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-badge uk-badge-success">yes</span>
                            <span class="uk-badge uk-badge-danger">No</span>

                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Features</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-badge uk-badge-success">LTE</span>
                            <span class="uk-badge uk-badge-primary">Quad HD</span>
                            <span class="uk-badge uk-badge-success">Android™ 5.0</span>
                            <span class="uk-badge uk-badge-success">64GB</span>

                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Objective</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">Sakani</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">finishing</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">Super Lux</span>
                        </div>
                    </div>

                </div>
                <div class="uk-width-large-1-2">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Price</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle"><a href="#">20000</a></span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Area</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">200</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Room num</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">200</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">bath room</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">200</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Public Services</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">200</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Payment type</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-middle">Cash</span>
                        </div>
                    </div>
                    <hr class="uk-grid-divider">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-large-1-3">
                            <span class="uk-text-muted uk-text-small">Notes</span>
                        </div>
                        <div class="uk-width-large-2-3">
                            <span class="uk-text-large uk-text-small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam necessitatibus suscipit velit voluptatibus! Ab accusamus</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="md-card-content uk-grid">
        <div class=" uk-width-medium-1-2 uk-margin-medium-bottom">
        <ul class="md-list ">
        <li>
        <div class="md-list-content">
        <span class="md-list-heading">Property Type</span>
        <span class="uk-text-medium  uk-text-truncate">Villa</span>
    </div>
</li>

<li>
<div class="md-list-content">
<span class="md-list-heading">Purpose</span>
<span class="uk-text-medium  uk-text-truncate">Sale</span>
</div>
</li>

<li>
<div class="md-list-content">
<span class="md-list-heading">City</span>
<span class="uk-text-medium  uk-text-truncate">Villa</span>
</div>
</li>

<li>
<div class="md-list-content">
<span class="md-list-heading">District</span>
<span class="uk-text-medium  uk-text-truncate">Villa</span>
</div>
</li>

<li>
<div class="md-list-content">
<span class="md-list-heading">Compound</span>
<span class="uk-text-medium  uk-text-truncate">Villa</span>
</div>
</li>
<li>
<div class="md-list-content">
<span class="md-list-heading">feature</span>
<span class="uk-text-medium  uk-text-truncate">Villa</span>
</div>
</li>
</ul>
</div>
</div> -->
</div>
</div>

</div>
