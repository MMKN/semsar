<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">

    <title>Semsar Online</title>


    <!-- uikit -->
    <link rel="stylesheet" href="../bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="../assets/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="../assets/css/main.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->
</head>
<body class="">

    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <div class="main_logo_top">
                    <a href="index.html"><img src="assets/img/white.png" alt="" height="15" width="71"/></a>
                </div>
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->
                <div class="uk-navbar-flip" style="display:none">
                    <!-- register button begin -->
                    <a class="md-btn md-btn-warning" data-uk-modal="{target:'#register_modal'}" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> Register / Login with facebook</a>
                    <!-- register model begin -->
                    <div class="uk-modal" id="register_modal">
                        <div class="uk-modal-dialog">
                            <button type="button" class="uk-modal-close uk-close"></button>
                            <p>There will be a button here bta3 el fb login / register</p>
                        </div>
                    </div>
                </div>            
                <div class="uk-navbar-flip">
                    <a class="md-btn md-btn-warning" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> My account</a>
                </div>    
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <!-- notification begin -->
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge" style="background:#f00">1</span></a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    </ul>
                                    <ul id="" class="uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <li style="cursor:pointer">
                                                    <div class="md-list-addon-element">
                                                        <i class="material-icons md-36 uk-text-warning">new_releases</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><a href="#">We have found you new matching properties</a></span>

                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="#" class="md-btn md-btn-primary js-uk-prevent">Check it now</a>
                                            </div>
                                        </li> 
                                    </ul>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            </form>
        </div>
    </header><!-- main header end -->

    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
    
        <div class="heading_actions">

            <div class="md-top-bar-actions-right">
                <div class="md-top-bar-icons">
                    <i id="mailbox_list_split" class=" md-icon material-icons">&#xE8EE;</i>
                    <i id="mailbox_list_combined" class="md-icon material-icons">&#xE8F2;</i>
                </div>
            </div>
            <a href="#" data-uk-tooltip="{pos:'bottom'}" title="Archive"><i class="md-icon material-icons">&#xE149;</i></a>
            <div data-uk-dropdown>
                <i class="md-icon material-icons">&#xE5D4;</i>
                <div class="uk-dropdown uk-dropdown-small">
                    <ul class="uk-nav">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Other Action</a></li>
                        <li><a href="#">Other Action</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <form id="filter_form">
            <div class="uk-grid">
                
                <h1 class=" uk-width-2-10 " style="margin-top:8px;"><i class="md-icon material-icons uk-margin-right">&#xE7F4;</i>Notifications</h1>
               <!--  <div class="uk-width-2-10 uk-tab-center " style="border-bottom:0px">
                    <ul class="uk-subnav uk-subnav-pill uk-margin-remove">
                        <li class="uk-active"><a href="#">All</a></li>
                        <li><a href="#">Call requests</a></li>
                        <li><a href="#">Customers</a></li>
                    </ul>
                </div> -->

                <div class="uk-width-medium-1-10">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="val_select" required data-md-selectize>
                            <option value="all" disabled selected>All</option>
                            <option value="call_request">Call requests</option>
                            <option value="customer_notice">Customers Notices</option>
                            <option value="Property_notice">Properties Notices</option>

                        </select>
                    </div>
                </div>
                <div class="uk-width-medium-1-10">
                    <div class="parsley-row  uk-margin-small-top">
                        <select id="status" name="status" required data-md-selectize>
                            <option value="" disabled selected>All</option>
                            <option value="1">Read</option>
                            <option value="0">Unread</option>

                        </select>
                    </div>
                </div>
                   
                <div class="uk-grid uk-width-4-10" data-uk-grid-margin>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_start">Start Date</label>
                            <input class="md-input" type="text" id="uk_dp_start" name="start_date">
                        </div>
                    </div>
                    <div class="uk-width-large-1-2 uk-width-medium-1-1">
                        <div class="uk-input-group">
                            <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                            <label for="uk_dp_end">End Date</label>
                            <input class="md-input" type="text" id="uk_dp_end" name="end_date">
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-2-10">
                    <div class="uk-input-group">
                       <span class="uk-input-group-addon">
                           <a href="#"><i class="material-icons">search</i></a>
                       </span>
                        <label for="contact_list_search">Search for anything!</label>
                        <input class="md-input" type="text"/>
                       
                    </div>

                   
                </div> 
                    
                
            </div>
        </form>   
    </div>
    


    <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card-list-wrapper" id="mailbox">
                <div class="uk-width-large-8-10 uk-container-center notification_container">

                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Today</div>
                        <div class="md-card-list-header md-card-list-header-combined heading_list" style="display: none">All Messages</div>
                        <ul class="hierarchical_slide">
                        @if(count($today))
                            @foreach($today as $notification)
                                <li>
                                    <!-- <div class="md-card-list-item-menu" >
                                        <a href="#" class="md-icon material-icons">done</a>
                                    </div> -->
                                    <span class="md-card-list-item-date " style="width:60px;"> {{ date("j M" , strtotime($notification->created_at)) }} </span>
                                    {{ $notification->getStatus($notification->read) }}
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck />
                                    </div>
                                    <!-- <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> -->
                                    <div class="md-card-list-item-sender">
                                        <span>Customer Name/ Property Name</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>Customer Name/ Property Name</span>
                                        </div>
                                        <span>Requested a Call / New Notice.</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">
                                            Earum maxime ratione itaque et consequatur sed laborum sed mollitia architecto laudantium ipsa porro molestiae accusamus numquam corrupti voluptatum quia eum blanditiis et molestiae illum explicabo suscipit autem non porro voluptatem molestiae atque est molestias hic officiis quisquam velit quis sint et consequuntur et consequuntur sit dolores non necessi
                                        </div>
                                        <div class="md-card-list-item-content">
                                            here is a timeline with any updates regarding the notification
                                            <div class="timeline">
                                                <div class="timeline_item">
                                                    <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                                                    <div class="timeline_date">
                                                        21 <span>Sep</span>
                                                    </div>
                                                    <div class="timeline_content">
                                                        Something about the customer goes here something about the customer goes here!


                                                    </div>
                                                </div>

                                            </div>                         
                                        </div>
                                        <form class="md-card-list-item-reply uk-container-center ">
                                            <label for="mailbox_reply_7254">Add update</label>
                                            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
                                            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                                        </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Today </h5> </li>
                        @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Yesterday</div>
                        <ul class="hierarchical_slide">
                            @if(count($yesterday))
                                @foreach($yesterday as $notification)
                                    <li>
                                       <!-- <div class="md-card-list-item-menu" >
                                           <a href="#" class="md-icon material-icons">done</a>
                                       </div> -->
                                       <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                       {{ $notification->getStatus($notification->read) }}
                                       <div class="md-card-list-item-select">
                                           <input type="checkbox" data-md-icheck />
                                       </div>
                                       <!-- <div class="md-card-list-item-avatar-wrapper">
                                           <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                       </div> -->
                                       <div class="md-card-list-item-sender">
                                           <span>Customer Name/ Property Name</span>
                                       </div>
                                       <div class="md-card-list-item-subject">
                                           <div class="md-card-list-item-sender-small">
                                               <span>Customer Name/ Property Name</span>
                                           </div>
                                           <span>Requested a Call / New Notice.</span>
                                       </div>
                                       <div class="md-card-list-item-content-wrapper">
                                           <!-- here goes description if exist  -->
                                           <div class="md-card-list-item-content">
                                               Earum maxime ratione itaque et consequatur sed laborum sed mollitia architecto laudantium ipsa porro molestiae accusamus numquam corrupti voluptatum quia eum blanditiis et molestiae illum explicabo suscipit autem non porro voluptatem molestiae atque est molestias hic officiis quisquam velit quis sint et consequuntur et consequuntur sit dolores non necessi
                                           </div>
                                           <div class="md-card-list-item-content">
                                               here is a timeline with any updates regarding the notification
                                               <div class="timeline">
                                                   <div class="timeline_item">
                                                       <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                                                       <div class="timeline_date">
                                                           21 <span>Sep</span>
                                                       </div>
                                                       <div class="timeline_content">
                                                           Something about the customer goes here something about the customer goes here!


                                                       </div>
                                                   </div>

                                               </div>                         
                                           </div>
                                           <form class="md-card-list-item-reply uk-container-center ">
                                               <label for="mailbox_reply_7254">Add update</label>
                                               <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
                                               <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                                           </form>
                                       </div>
                                    </li>
                                @endforeach
                            @else
                                <li> <h5 style="text-align:center; margin:0; padding:0;"> No Notifications For Yesterday </h5> </li>
                            @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">This Month</div>
                        <ul class="hierarchical_slide">
                        @if(count($month))
                            @foreach($month as $notification)
                                <li>
                                    <!-- <div class="md-card-list-item-menu" >
                                        <a href="#" class="md-icon material-icons">done</a>
                                    </div> -->
                                    <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                    {{ $notification->getStatus($notification->read) }}
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck />
                                    </div>
                                    <!-- <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> -->
                                    <div class="md-card-list-item-sender">
                                        <span>Customer Name/ Property Name</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>Customer Name/ Property Name</span>
                                        </div>
                                        <span>Requested a Call / New Notice.</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">
                                            Earum maxime ratione itaque et consequatur sed laborum sed mollitia architecto laudantium ipsa porro molestiae accusamus numquam corrupti voluptatum quia eum blanditiis et molestiae illum explicabo suscipit autem non porro voluptatem molestiae atque est molestias hic officiis quisquam velit quis sint et consequuntur et consequuntur sit dolores non necessi
                                        </div>
                                        <div class="md-card-list-item-content">
                                            here is a timeline with any updates regarding the notification
                                            <div class="timeline">
                                                <div class="timeline_item">
                                                    <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                                                    <div class="timeline_date">
                                                        21 <span>Sep</span>
                                                    </div>
                                                    <div class="timeline_content">
                                                        Something about the customer goes here something about the customer goes here!


                                                    </div>
                                                </div>

                                            </div>                         
                                        </div>
                                        <form class="md-card-list-item-reply uk-container-center ">
                                            <label for="mailbox_reply_7254">Add update</label>
                                            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
                                            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                                        </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <h1>No For Month</h1>
                        @endif
                        </ul>
                    </div>


                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Older Messages</div>
                        <ul class="hierarchical_slide">
                        @if(count($notifications))
                            @foreach($notifications as $notification)
                                <li>
                                    <!-- <div class="md-card-list-item-menu" >
                                        <a href="#" class="md-icon material-icons">done</a>
                                    </div> -->
                                    <span class="md-card-list-item-date " style="width:60px;">{{ date("j M" , strtotime($notification->created_at)) }}</span>
                                    {{ $notification->getStatus($notification->read) }}
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck />
                                    </div>
                                    <!-- <div class="md-card-list-item-avatar-wrapper">
                                        <img src="assets/img/avatars/avatar_01_tn.png" class="md-card-list-item-avatar" alt="" />
                                    </div> -->
                                    <div class="md-card-list-item-sender">
                                        <span>Customer Name/ Property Name</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>Customer Name/ Property Name</span>
                                        </div>
                                        <span>Requested a Call / New Notice.</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <!-- here goes description if exist  -->
                                        <div class="md-card-list-item-content">
                                            Earum maxime ratione itaque et consequatur sed laborum sed mollitia architecto laudantium ipsa porro molestiae accusamus numquam corrupti voluptatum quia eum blanditiis et molestiae illum explicabo suscipit autem non porro voluptatem molestiae atque est molestias hic officiis quisquam velit quis sint et consequuntur et consequuntur sit dolores non necessi
                                        </div>
                                        <div class="md-card-list-item-content">
                                            here is a timeline with any updates regarding the notification
                                            <div class="timeline">
                                                <div class="timeline_item">
                                                    <div class="timeline_icon timeline_icon_primary"><a href="#" onclick="UIkit.modal.prompt('', '', function(val){ UIkit.modal.alert('Hello '+(val || 'Mr noname')+'!'); });"><i class="material-icons">edit</i></a></div>
                                                    <div class="timeline_date">
                                                        21 <span>Sep</span>
                                                    </div>
                                                    <div class="timeline_content">
                                                        Something about the customer goes here something about the customer goes here!


                                                    </div>
                                                </div>

                                            </div>                         
                                        </div>
                                        <form class="md-card-list-item-reply uk-container-center ">
                                            <label for="mailbox_reply_7254">Add update</label>
                                            <textarea class="md-input md-input-full" name="mailbox_reply_7254" id="mailbox_reply_7254" cols="30" rows="1"></textarea>
                                            <button class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                                        </form>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <h1>No Notifications</h1>
                        @endif
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>



    

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="#mailbox_new_message" data-uk-modal="{center:true}">
            <i class="material-icons">add</i>
        </a>
    </div>

    <div class="uk-modal" id="mailbox_new_message">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            <form>
                <div class="uk-modal-header">
                    <h3 class="uk-modal-title">Compose Message</h3>
                </div>
                <div class="uk-margin-medium-bottom">
                    <label for="mail_new_to">To</label>
                    <input type="text" class="md-input" id="mail_new_to"/>
                </div>
                <div class="uk-margin-large-bottom">
                    <label for="mail_new_message">Message</label>
                    <textarea name="mail_new_message" id="mail_new_message" cols="30" rows="6" class="md-input"></textarea>
                </div>
                <div id="mail_upload-drop" class="uk-file-upload">
                    <p class="uk-text">Drop file to upload</p>
                    <p class="uk-text-muted uk-text-small uk-margin-small-bottom">or</p>
                    <a class="uk-form-file md-btn">choose file<input id="mail_upload-select" type="file"></a>
                </div>
                <div id="mail_progressbar" class="uk-progress uk-hidden">
                    <div class="uk-progress-bar" style="width:0">0%</div>
                </div>
                <div class="uk-modal-footer">
                    <a href="#" class="md-icon-btn"><i class="md-icon material-icons">&#xE226;</i></a>
                    <button type="button" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="../assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="../assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="../assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->

    <!--  mailbox functions -->
    <script src="../assets/js/pages/page_mailbox.min.js"></script>
    <!--  forms advanced functions -->
    <script src="../assets/js/pages/forms_advanced.min.js"></script>
    
    <!-- page specific plugins -->
    <!-- ionrangeslider -->
    <script src="../bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>


    <script>
    var base_url = '{{ URL::route("profile.main") }}';
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>
<script type="text/javascript" src="../js/notifications.js"></script>

</body>
</html>
    