<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">

    <title>{{trans('main.project_name')}}</title>

    <script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/underscore.js/1.1.4/underscore-min.js"></script>
    <script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/backbone.js/0.3.3/backbone-min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js" type="text/javascript"></script>
    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="assets/css/main.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
    <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->

</head>
<body class="top_menu">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">

                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons uk-margin-large-right md-24">info</i><i class="material-icons md-24">call</i> 01123777760</a>

                        <div class="uk-dropdown uk-dropdown-width-1">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-1-1">
                                    <div class="uk-grid  uk-grid-width-medium-1-1 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                       
                                        <a href="http://minionz.net" target="_blank">
                                            <span class="uk-text-muted uk-display-block uk-margin-bottom">Developed by</span>
                                            <img src="assets/img/favicon.png" alt=""     height="15" width="71"/>
                                            <span class="uk-text-muted uk-display-block uk-margin">Minionz</span>
                                            

                                        </a>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="main_logo_top">
                    <a href="index.html"><img src="" alt="" height="15" width="71"/></a>
                </div > -->
                <!-- if the user is not logged in this button will appear and once he loged in it should disappear -->
                <div class="uk-navbar-flip" style="display:none">
                    <!-- register button begin -->
                    <a class="md-btn md-btn-warning" data-uk-modal="{target:'#register_modal'}" href="#" style="margin-top: 6px;"><i class="material-icons md-24 md-light">person</i> {{trans('main.sing_in')}}</a>
                    <!-- register model begin -->
                    <div class="uk-modal" id="register_modal">
                        <div class="uk-modal-dialog">
                            <button type="button" class="uk-modal-close uk-close"></button>
                            <p>There will be a button here bta3 el fb login / register</p>
                        </div>
                    </div>

                </div>
                <div class="uk-navbar-flip" data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                <a class="md-btn md-btn-warning" href=" {{ URL::route('properties.main') }} " style="margin-top: 6px;"><i class="material-icons md-24 md-light">home</i> {{trans('main.Home')}} </a>

    </div>
    <div class="uk-navbar-flip">
        <ul class="uk-navbar-nav user_actions">
            <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
            <!-- notification begin -->
            @if(count($notifications))
            <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                <a href="#" class="user_action_icon">
                    <i class="material-icons md-24 md-light">&#xE7F4;</i>
                    <span class="uk-badge" style="background:#f00">
                        {{ count($unread_notifications) or '' }}
                    </span>
                </a>
                <div class="uk-dropdown uk-dropdown-xlarge">
                    <div class="md-card-content">
                        <ul data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                        </ul>
                        <ul class="uk-margin">
                            @if(count($notifications))
                                <li>
                                    <ul class="md-list md-list-addon">
                                        @foreach($notifications->take(5) as $notification)
                                            <li style="cursor:pointer">
                                                <div class="md-list-addon-element">
                                                    <i class="material-icons md-36 uk-text-warning">new_releases</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">
                                                        <a href="{{ URL::route('property' , $notification->property_id) }}" target=_blank>
                                                            We have found you new matching property
                                                        </a>
                                                    </span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <!-- <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                        <a href="#" class="md-btn md-btn-primary js-uk-prevent">Check it now</a>
                                    </div> -->
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </li>
            @endif

        </ul>
    </div>

</nav>
</div>
<div class="header_main_search_form">
    <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
    <form class="uk-form">
        <input type="text" class="header_main_search_input" />
        <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
    </form>
</div>
</header><!-- main header end -->


<div id="page_content">

    <!-- filter start -->
    <div id="page_heading" style="display:none;">

    </div>
    <!-- filter end -->

    <div id="page_content_inner">
        <div class="uk-grid">
            <div id="profile_main_section" class="uk-width-large-10-10 uk-container-center">
                @include('profile.grid')
</div>

<div id="profile_property_section" class="uk-width-large-10-10 uk-container-center">

</div>



</div>


<div id="profile_info_section" class="uk-grid uk-width-large-10-10 uk-container-center" style="display:none;">


    <!-- user Preferences begin -->
    <div class="uk-width-large-6-10 uk-container-center">
            <div class="md-card user_content">
                <h4 class="full_width_in_card heading_c uk-text-center">{{trans('main.Personal_information')}}</h4>
                <div class="md-card-content">
                    <div class="uk-grid  uk-grid-medium">
                        <div class="uk-width-large-1-1">
                            <form id="profile_form" method="post">


                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">{{trans('main.Email')}}</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <span class="uk-text-large uk-text-middle">{{$user->email}}</span>
                                    </div>
                                </div>

                                <div class="uk-width-medium-10-10 parsley-row" style="margin-top:20px;">
                                    <div class="">
                                        <label>{{trans('main.Full_Name')}}</label>
                                        <input type="text" class="md-input" name="name" value="{{$user->name}}" data-parsley-id="4"/>
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>


                                <!-- <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Name</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <input name="name" value="{{$user->name}}" class="md-input masked_input" type="text" />
                                    </div>
                                </div> -->

                                <div class="uk-width-medium-10x-10 parsley-row" style="margin-top:20px;">
                                    <div class="">
                                        <label for="wizard_address">{{trans('main.Mobile_main')}}</label>
                                        <input name="mobile" value="{{$user->mobile}}" class="md-input masked_input" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" type="text"/>
                                        <span class="md-input-bar"></span>
                                    </div>
                                </div>


                                <!-- <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3">
                                        <span class="uk-text-muted uk-text-small">Mobile</span>
                                    </div>
                                    <div class="uk-width-large-2-3">
                                        <input name="mobile" value="{{$user->mobile}}" class="md-input masked_input" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false"  type="text"/>
                                    </div>
                                </div> -->
                                <br/>
                                <button id="profile_save" class="md-btn md-btn-primary">{{trans('main.Save')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="md-fab-wrapper" onclick="back()">
        <a class="md-fab md-fab-warning">
            <i class="material-icons"></i>
        </a>
    </div>
</div>

</div>

</div>
</div>
</div>

<div class="uk-modal" id="mobile_modal">
    <div class="uk-modal-dialog">
        <form class="uk-form-stacked">
            <div class="uk-margin-medium-bottom">
                <label for="task_title">You Must provide mobile number to request a call</label>
                <input type="text" class="md-input" id="user_mobile" name="user_mobile" data-inputmask="'mask': '999 - 999 99 999'" data-inputmask-showmaskonhover="false" />
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" onclick="submitMobile()" class="md-btn md-btn-flat md-btn-flat-primary" id="save_mobile">Submit</button>
            </div>
        </form>
    </div>
</div>

<!-- google web fonts -->

<script>
WebFontConfig = {
    google: {
        families: [
            'Source+Code+Pro:400,700:latin',
            'Roboto:400,300,500,700,400italic:latin'
        ]
    }
};
(function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
</script>

<!-- common functions -->
<script src="assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="assets/js/uikit_custom.min.js"></script>
<!-- altair common functions/helpers -->
<script src="assets/js/altair_admin_common.min.js"></script>
<script src="bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
<!-- inputmask-->
<script src="bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>

<!--  forms advanced functions -->
<script src="assets/js/pages/forms_advanced.min.js"></script>

<script>
$(function() {
    // enable hires images
    altair_helpers.retina_images();
    // fastClick (touch devices)
    if(Modernizr.touch) {
        FastClick.attach(document.body);
    }
});
</script>
<script>
var base_url = '{{ URL::route("profile.main") }}';
var base_base_url = '{{ URL::to("/") }}';
$(function(){
    var last_scroll=0 ;
    $(window).scroll(function() {
        var current_scroll=$(this).scrollTop();
        if(current_scroll < last_scroll ){
            if(current_scroll < 60)
            $("#header_main").slideDown("280");
        }
        else if( current_scroll > 72) {
            $("#header_main").slideUp("280");
        }
        last_scroll=current_scroll;
    });
})


</script>
<script type="text/javascript" src="js/general.js"></script>
<script type="text/javascript" src="js/profile.js"></script>
</body>
</html>
