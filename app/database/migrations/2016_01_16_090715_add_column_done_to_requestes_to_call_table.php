<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDoneToRequestesToCallTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('requests_to_call', function(Blueprint $table)
		{
			$table->boolean('done');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('requests_to_call', function(Blueprint $table)
		{
			$table->dropColumn('done');
		});
	}

}
