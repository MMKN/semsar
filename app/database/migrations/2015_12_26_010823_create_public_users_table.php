<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('public_users' , function(Blueprint $table){
				$table->bigIncrements('id');
				$table->string('name');
				$table->string('email')->unique();
				$table->string('mobile');
				$table->integer('purpose');          // 0 sale - 1 rent
				$table->integer('unit_type');
				$table->integer('city');
				$table->integer('neighbour');
				$table->integer('price_from');
				$table->integer('price_to');
				$table->integer('area');
				$table->integer('floor');
				$table->integer('objective');       // office - home
				$table->integer('payment_type');
				$table->integer('finishing');
				$table->boolean('is_compound');
				$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('public_users');
	}

}
