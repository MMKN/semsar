<?php
return array(
		'project_name' => 'Real Estate',
		'properties' => 'Properties',
		'customers' => 'Customers',
		'settings' => 'Settings',
		'Logout' => 'Logout',
		'Save' => 'Save',
		'Delete' => 'Delete',
		'Cancel' => 'Cancel',
		'Archieved_properties' => 'Archieved properties',
		'Sale' => 'Sale',
		'Rent' => 'Rent',
		'Property_type' => 'Property type',
		'City' => 'City',
		'District' => 'District',
		'Floor' => 'Floor',
		'Compound' => 'Compound',
		'Published' => 'Published',
		'Finishing' =>'Finishing',
		'Payment_style' => 'Payment style',
		'Objective' => 'Objective',
		'Price_from' => 'Price from',
		'Price_to' => 'Price to',
		'Area' => 'Area',
		'Rooms_No' => 'Rooms numbers',
		'Toilets_No' => 'Toilets numbers',
		'Distance_Facilites' => 'Installment',
		'Reset' => 'Reset',
		'No_Properies_Available' => 'No Properies Available',
		'Add_New_Property' =>'Add New Property',
		'Edit_Property' =>'Edit Property',
		'Street_No_landmarks' => 'Street No. / Landmarks',
		'Plot_no' => 'Plot Number',
		'Apartment_no' => 'Apartment Number',
		'Price' => 'Price',
		'Public_services' =>'Installment',
		'Description' => 'Description',
		'Features' => 'Features',
		'Location' => 'Location',
		'Latitude'=> 'Latitude',
		'Longitude' => 'Longitude',
		'Photos' => 'Photos',
		'Choose_file' => 'Choose file',
		'Personal_information' => 'Personal information',
		'Full_Name' => 'Full name',
		'Email' => 'Email',
		'Mobile_main' => 'Mobile main',
		'Other_phones' => 'Other Numbers',
		'Olx' => 'Olx',
		'Facebook' => 'Facebook',
		'word_of_mouth' => 'Word of mouth',
		'Others' => 'Others..',
		'Are_you_sure_delete' => 'Are you sure that u wanna delete this property?',
		'main_image' => 'Main image',
		'No_Properies_Available' => 'No Properies Available',
		'Exact_Properties' => 'Exact Properties',
		'Favourite_Properties' => 'Favourite Properties',
		'Price_Properties' => 'Properties With The Same Price',
		'Suggested_Properties' => 'Suggested Properties',
		'No_Exact_Properties' => 'No Matching Properties',
		'unarchieve' => 'unarchieve',
		'Timeline' => 'Timeline',
		'Other_Informations' => 'Other Informations',
		'Other_Information' => 'Other_Information',
		'Basic_Info' => 'Main Info',
		'Images' => 'Images',
		'Property_preferences' => 'Property preferences',
		'Purpose' => 'purpose',
		'Owner_Info' => 'Owner Information',
		'Date_added' => 'Date added',
		'Heard_us_by' => 'Heard us by',
		'property_address' => 'property address',
		'Matching_Customers' => 'Matching Customers',
		'Similar_Customers' => 'Similar Customers',
		'Price_Customers' => 'Price Match',
		'No_data_found' => 'No data found',
		'Archieved_Customers' => 'Archieved Customers',
		'customer_search' =>'Find user by name / mobile / email',
		'No_Customers' => 'No Customers Available',
		'customer_delete' => 'Are you sure you wanna archieve this customer?',
		'Suitable_properties' =>'Suitable properties',
		'Similar_Properties' => 'Similar Properties',
		'customer_preferences' => 'Customer preferences',
		'sms' => 'Send Sms with URL',
		'Link_Active' => 'Login link activation',
		'Users' => 'Users',
		'Filter' => 'Configurations',
		'City_districts' => 'City / districts',
		'Groups' => 'Advanced Search',
		'Data_Export' => 'Data Export',
		'Manage_system_users' => 'Manage system users',
		'General_settings' => 'General settings' ,
		'Floors' => 'Floors',
		'Area_margin' => 'Area margin',
		'Rooms_margin' => 'Rooms margin',
		'Toilets_margin' => 'Toilets margin',
		'Manage_Cities_districts' => 'Manage Cities and districts',
		'Manage_Groups' => 'Manage Groups',
		'Export_to' => 'Export Data to Excel sheets',
		'Download_Properties' => 'Download Properties',
		'Download_Customers' => 'Download Customers',
		'Group_name' => 'Group name',
		'type' => 'Type',
		'add_new' => 'Add new customer with this preferences',
		'Agent' => 'Agent',
		'Admin' => 'Admin',
		'Exact_filter_margins' => 'Exact filter margins',
		'Similar_filter_margins' => 'Similar filter margins',
		'show' => 'show',
		'Password' => 'Password',
		'Sign_In' => 'Sign In',
		'Yes' => 'yes',
		'No' => 'no',
		'يث' => 'Search',
		'write_message' => 'Write Notes here',
		'Are_you_sure_delete_property' => 'Are you sure delete this property',
		'maximize' => 'Full screen',
		'All' => 'All',
		'Language' => 'Language',
		'disconnect' => 'Disconnect / Change Google Account',
		'legal' => 'Legal Status of the owner',
		'show_map' => 'Show Map',
		'no_match' => 'No matching',
		'add_task' => 'Add Task',
		'VIP_Customers' =>'System Customers',
		'website_customers' => 'Website Users',
		'No_Results_Found' => 'No Results Found',
		'Key' => 'ID',
		'Title' => 'Title',
		'Assignee' => 'Assignee',
		'Priority' => 'Priority',
		'Created' => 'Created at',
		'Updated' => 'Last Update',
		'Status' => 'Status',
		'Type' => 'Type',
		'me' => 'Me',
		'Close' => 'Cancel',
		'Add' => 'Add',
		'Edit' => 'Edit',
		'see_all_notification' => 'All Notification',
		'Notifications' => 'Notifications',
		'Re-Open' => 'Re-Open',
		'Close_Issue' => 'Close Issue',
		'Comments' => 'Updates',
		'add_comment' => 'Add Update',
		'Details' => 'Details',
		'All_Assignees' => 'All Assignees',
		'All_types' => 'All types',
		'General' => 'General',
		'Call_requests' => 'Call requests',
		'Properties_Notices' => 'Properties Notices',
		'Customers_Notices' => 'Customers Notices',
		'Open' => 'Open',
		'Closed' => 'Closed',
		'Reopened' => 'Reopened',
		'All_Status' => 'All status',

		'Search_id' => 'Search wit id',
		'End_Date' => 'End Date',
		'Start_Date' => 'Start Date',
		'no_notifications' => 'No Notifications',


		'save_preferences' => 'Save as my preferences',
		'my_account' => 'My Account',
		'Request_call' => 'Request a call',
		'Add_to_fav' => 'Add to favourite',
		'view_edit_profile' => 'View / Edit my prefrences',
		'best_match' => 'Best match to your requirments',
		'personal_preferences' => 'Personal preferences',
		'Home' => 'Home',
		'sing_in' => 'Sing in / sing up with facebook',
		'remove_fav' => 'Delete from favourite',

		'back' => 'Back'

	);