<?php

class PaymentType extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'payment_styles';

	public function getPaymentStyle($id){
		return $this->whereId($id)->pluck('value');
	}

}
