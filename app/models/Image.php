<?php

class Image extends Eloquent{

	protected $table = 'images';

	public function getState($image_id){
		$image = Image::whereId($image_id)->whereMain(1)->get()->first();
		if(!$image) return '';

		return 'Checked';
	}
}
