<?php

class AdminNotification extends Eloquent{

	protected $table = 'admin_notifications';

	/**
	* Type 0 = general
	* Type 1 = Call Request
	* Type 2 = Property Notice
	* Type 3 = Customer Notice
	*/
	// protected $randomString = "";
	// public function __construct(){
	// 	$this->randomString = $this->generate_random_string(7);
	// }
	
	// protected $attributes = [
	//     'key' => $randomString
	// ];

	public function generate_random_string($name_length = 6)
	{
	    $alpha_numeric = 'abcdefghijklmnopqrstuvwxyz0123456789';
	    return substr(str_shuffle($alpha_numeric), 0, $name_length);
	}

}
