<?php

class City extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'cities';

	public static $rules = [
		'name'   => 'required|unique:cities'
	];

	public function getCity($id)
	{
		return $this->whereId($id)->pluck('name');
	}


	public function getNeighbourOfCity($city_id)
	{
		return Neighbour::whereCity_id($city_id)->get();
	}
}
