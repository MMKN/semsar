<?php

class PropertyFeature extends Eloquent{

	protected $table = 'property_features';

	public function getFeature($id){
		return $this->whereId($id)->pluck('value');
	}

}
