<?php

class UnitType extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'unit_types';

	public function getUnitType($id){
		return $this->whereId($id)->pluck('value');
	}

}
