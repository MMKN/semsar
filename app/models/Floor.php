<?php

class Floor extends Eloquent{


	use SoftDeletingTrait;
	protected $table = 'floors';

	public function getFloor($id){
		return $this->whereId($id)->pluck('value');
	}

}
