<?php

class Group extends Eloquent{

	protected $table = 'groups';

	public function getType($type)
	{
		return ($type == 0) ? 'Districts' : 'Floors';
	}

	public function getDistricts($id)
	{
		return NeighbourGroup::whereGroup_id($id)->get();
	}

	public function getFloors($id)
	{
		return FloorGroup::whereGroup_id($id)->get();
	}


}
