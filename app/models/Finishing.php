<?php

class Finishing extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'finishing';

	public function getFinishing($id){
		return $this->whereId($id)->pluck('value');
	}

}
