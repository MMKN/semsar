<?php

class Notification extends Eloquent{

	protected $table = 'notifications';

	public function getStatus($status)
	{	
		if ($status == 0) {
			return '<span class="md-card-list-item-date uk-badge uk-badge-danger uk-margin-right" style="color:#fff">Open</span>';
		}

		return '<span class="md-card-list-item-date uk-badge uk-badge-success uk-margin-right" style="color:#fff">Done</span>';
	}
}
