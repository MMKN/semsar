<?php

class Objective extends Eloquent{

	use SoftDeletingTrait;
	protected $table = 'objectives';

	public function getObjective($id){
		return $this->whereId($id)->pluck('value');
	}

}
