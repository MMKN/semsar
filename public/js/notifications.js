$("#filter_form").change(function(e){
	e.preventDefault();
	var formdata = $("#filter_form").serialize();
	$.ajax({
		type : 'GET',
		url  : base_url + '/notification-filter',
		data : formdata,
		success : function(data){
			$(".notification_container").html(data);
		},
        error: function(data)
        {
        
        }
	});
});
