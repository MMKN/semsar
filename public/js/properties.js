<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--////////////////////////// Filter Start \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->

$(document).on('click', '#fb_button', function() {
    altair_helpers.content_preloader_show();
});
// handle filter button
$(document).on('click', '#filter_search_button', function() {
    filter();
});

// handle reset button
$(document).on('click', '#filter_reset_button', function() {
    reset_filter();
});



function filter() {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: $('#filter_form').prop('action'),
        data: $('#filter_form').serialize(),
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $('#page_content_inner').html(data['result']);
        }
    });
}

function reset_filter() {
    altair_helpers.content_preloader_show();
    resetForm()
    $.ajax({
        type: 'POST',
        url: base_url + '/reset',
        success: function(data) {
            altair_helpers.content_preloader_hide();
            $('#page_content_inner').html(data['result']);

        }
    });
}

function save_filter() {

    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'POST',
        url: base_url + '/save',
        data: $('#filter_form').serialize(),
        success: function(data) {
            if (data == 2) {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: 'Please, Login In First!',
                    pos: 'top-center',
                    status: 'danger'
                });
            } else {
                altair_helpers.content_preloader_hide();
                UIkit.notify({
                    message: 'Your Prefrences Has Been Updated Successfuly.',
                    pos: 'top-center',
                    status: 'success'
                });
            }
        }
    });
}

// reset filter form
function resetForm() {
    $("#filter_unit_type")[0].selectize.clear();
    $("#filter_city")[0].selectize.clear();
    $("#filter_neighbours")[0].selectize.clear();
    $('#floor_search')[0].selectize.clear();
    $('#objective_search')[0].selectize.clear();
    $('#payment_search')[0].selectize.clear();
    $('#finishing_search')[0].selectize.clear();

    if ($("#compound").is(":checked")) {
        $("#compound").click();
    }
    $('.ichk').iCheck('uncheck');
    $("#sale").iCheck('check');
    $("#filter_form")[0].reset();
    $(".md-input-success").each(function() {
        $(this).removeClass('md-input-success');
    });

    $(".md-input-filled").each(function() {
        $(this).removeClass('md-input-filled');
    });
}

// get a list from db on select change event
// pass the id of component will be affected and value of the select
// the result will be in the variable "returned_data" in the global object
$('#filter_city').change(function() {
    altair_helpers.content_preloader_show();
    $("#filter_neighbours").attr('disabled', false);
    getListChange('#filter_neighbours_container', '/neighbour', this.value);
    $(".selectize-input input").attr('readonly', 'readonly');

});

function getListChange(component_id, url, value) {
    $.ajax({
        type: 'GET',
        async: false,
        url: base_url + url,
        data: {
            city_id: value
        },
        success: function(data) {
            $(component_id).html(data['result']);
            altair_forms.init();
            altair_helpers.content_preloader_hide();
        }
    });
}

<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--////////////////////////// Filter End \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->


<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////// Grid Start \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->

// handle pagination change
function getPage(elem, url) {
    altair_helpers.content_preloader_show();
    $.ajax({
        type: 'GET',
        url: base_url + '/paginated?page=' + url,
        data: {},
        success: function(data) {
            setTimeout(function() {
                altair_helpers.content_preloader_hide();
            }, 1000);
            altair_md.card_show_hide($("#page_content_inner"), void 0, function() {
                $("#page_content_inner").html(data);
                $("#page_content_inner , .pager").show();
            }, void 0);
        }
    });
}
window.onhashchange = function() {
    var what_to_do = document.location.hash;
    if (what_to_do == "")
        back();
    else {
        what_to_do = what_to_do.replace('#', '');
        viewProperty(parseInt(what_to_do));
        console.log(what_to_do);
    }
}
$(document).ready(function() {
    var what_to_do = document.location.hash;
    what_to_do = what_to_do.replace('#', '');
    if (parseInt(what_to_do)) {
        viewProperty(parseInt(what_to_do));
    }
});



function viewProperty(property_id) {
    altair_helpers.content_preloader_show();
    document.location.hash = property_id;
    $.ajax({
        type: 'GET',
        url: base_url + '/property',
        data: {
            property_id: property_id
        },
        success: function(data) {
            homepage = 1;
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            $('.uk-sticky-placeholder').slideUp();
            $('#page_heading').slideUp();
            $('#page_content_inner').slideUp();
            $('#property').html(data['result']).slideDown();
            altair_helpers.content_preloader_hide();
        }
    });
}

function back() {
    document.location.hash = "";
    $('#property').slideUp();
    $('.uk-sticky-placeholder').slideDown();
    $('#page_heading').slideDown();
    $('#page_content_inner').slideDown();
    homepage = 0;
    // altair_helpers.content_preloader_show();
    // $.ajax({
    //     type : 'GET',
    //     url  : base_url + '/grid',
    //     success: function(data)
    //     {
    //         homepage = 0;
    //         $('.uk-sticky-placeholder').slideDown();
    //         $('#page_heading').slideDown();
    //         $('#page_content_inner').html(data['result']);
    //         altair_helpers.content_preloader_hide();
    //     }
    // });
}
<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////// Grid End \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
<!--/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
/////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->*

function submitMobile() {
    var mobile = $("#user_mobile").val();
    if (mobile == "" || mobile.length < 10) {
        $("#user_mobile").focus();
    } else {
        $.ajax({
            type: 'POST',
            url: base_url + '/submit-mobile',
            data: {
                mobile: mobile
            },
            success: function(data) {
                UIkit.modal("#mobile_modal").hide();
            }
        });
    }
}

$(document).on('ifChanged', '#sale', function() {
    if ($(this).is(':checked') && $(this).val() == '0') {
        $(".payment_hide").show();
        $('#price_to')[0].selectize.clear();
        $.ajax({
            type: 'GET',
            url: base_url + '/prices',
            data: {
                purpose: 0
            },
            success: function(data) {
                $('#price_from_selector').html(data['from']);
                $('#price_to_selector').html(data['to']);
                altair_forms.init();
                altair_md.inputs();
                $(".selectize-input input").attr('readonly', 'readonly');
            }
        });
    }
});

$(document).on('ifChanged', '#rent', function() {
    if ($(this).is(':checked') && $(this).val() == '1') {
        $(".payment_hide").hide();
        $('#price_to')[0].selectize.clear();
        $.ajax({
            type: 'GET',
            url: base_url + '/prices',
            data: {
                purpose: 1
            },
            success: function(data) {
                $('#price_from_selector').html(data['from']);
                $('#price_to_selector').html(data['to']);
                altair_forms.init();
                altair_md.inputs();
                $(".selectize-input input").attr('readonly', 'readonly');
            }
        });
    }
});

function updatePrices(elem) {
    return;
    var checked = 0;
    if ($("#rent").is(':checked')) {
        var checked = 1;
        console.log("yes");
    }
    $.ajax({
        type: 'GET',
        url: base_url + '/to',
        data: {
            purpose: checked,
            price: $(elem).val()
        },
        success: function(data) {
            $('#price_to_selector').html(data);
            altair_forms.init();
            altair_md.inputs();
            $(".selectize-input input").attr('readonly', 'readonly');

        }
    });
}