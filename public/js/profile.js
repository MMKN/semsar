$(document).ready(function(){
$("#profile_form").submit(function(e){
	e.preventDefault();
	var formdata = $("#profile_form").serialize();
	$.ajax({
		type : 'POST',
		url  : base_url + '/store',
		data : formdata,
		success : function(){
			UIkit.notify({message: 'Profile Updated successfully.' , pos: 'top-center' , status: 'success'});
		},
        error: function(data)
        {
        	var msg = data['responseJSON']['error']['message'];
            UIkit.notify({message: msg , pos: 'top-center' , status: 'danger'});
        }
	});
});
});

function viewPrefs(){
	altair_helpers.content_preloader_show();
			$.ajax({
				async:false,
		        type : 'GET',
		        url  : base_url + '/prefs',
		        data :
		        {

		        },
		        success: function(data)
		        {
		            $("#page_heading").html(data);
		            altair_md.inputs();
		            altair_forms.init();
		            altair_md.checkbox_radio();
		            $(":input").inputmask();
					$('#profile_main_section').slideUp();
					$("#page_heading").slideDown();
					$('#profile_info_section').slideDown();
					altair_helpers.content_preloader_hide();
		        }
		    });

}

function save_filter(){

    altair_helpers.content_preloader_show();
    $.ajax({
        type : 'POST',
        url  : base_base_url + '/save',
        data : $('#filter_form').serialize(),
        success: function(data)
        {
            altair_helpers.content_preloader_hide();
            UIkit.notify({message: 'Your Prefrences Has Been Updated Successfuly.' , pos: 'top-center' , status: 'success'});
        },
        error: function(){
            altair_helpers.content_preloader_hide();
            UIkit.notify({message: 'Please, Login In First!' , pos: 'top-center' , status: 'success'});
        }
    });
}

function getListChange(component_id , value){
    $.ajax({
        type : 'GET',
        async: false,
        url  : base_base_url + '/neighbour',
        data :
        {
            city_id : value
        },
        success: function(data)
        {
            $(component_id).html(data['result']);
            altair_forms.init();
            altair_helpers.content_preloader_hide();
        }
    });
}

function viewProperty(property_id){
    altair_helpers.content_preloader_show();
    $.ajax({
        type : 'GET',
        url  : base_url + '/property',
        data :
        {
            property_id : property_id
        },
        success: function(data)
        {
            $('#profile_main_section').slideUp();
            $('#profile_property_section').html(data['result']);
            $('#profile_property_section').slideDown();
            altair_helpers.content_preloader_hide();
        }
    });

    console.log("sadf");
}

function back(){
    altair_helpers.content_preloader_show();
    $.ajax({
        type : 'GET',
        url  : base_url + '/grid',
        success: function(data)
        {
            $('#profile_main_section').html(data);
            $('#profile_property_section').slideUp();
            $("#page_heading").slideUp();
            $('#profile_main_section').slideDown();
            $('#profile_info_section').slideUp();
            $('#profile_property_section').html('');
            altair_helpers.content_preloader_hide();
        }
    });
}

function submitMobile(){
    var mobile = $("#user_mobile").val();
    if(mobile == "" || mobile.length < 10){
        console.log($("#user_mobile").val() + " " +mobile.length);
        $("#user_mobile").focus();
    }else{
        $.ajax({
            type : 'POST',
            url  : base_url + '/submit-mobile',
            data :
            {
                mobile : mobile
            },
            success :function(data){
                UIkit.modal("#mobile_modal").hide();
            }
        });
    }
}