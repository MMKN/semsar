function add_remove_favourite(property_id){
    altair_helpers.content_preloader_show();
    $.ajax({
        type : 'POST',
        url  : base_url + '/favourite',
        data :
        {
            property_id : property_id
        },
        success :function(data){
            altair_helpers.content_preloader_hide();
            if(data == 0){
                UIkit.notify({message: 'This Property Has Been Removed From Your Favourites Successfuly.' , pos: 'top-center' , status: 'warning'});
                $('#favourite_text').html('Add To Favourites');
            }
            else if(data == 1){
                UIkit.notify({message: 'This Property Has Been Added To Your Favourites Successfuly.' , pos: 'top-center' , status: 'success'});
                $('#favourite_text').html('Remove From Favourites');
            }else{
                altair_helpers.content_preloader_hide();
                UIkit.notify({message: 'Please, Login In First!' , pos: 'top-center' , status: 'danger'});
            }
        }
    });
}


function add_request(property_id){
    altair_helpers.content_preloader_show();
    $.ajax({
        type : 'POST',
        url  : base_url + '/request',
        data :
        {
            property_id : property_id
        },
        success :function(data){
            altair_helpers.content_preloader_hide();
            if(data == 0){
                UIkit.notify({message: 'You Have Already Requested A Call For This Property Before.' , pos: 'top-center'  , status: 'warning'});
            }else if(data == 1){
                UIkit.notify({message: 'Your Request Has Been Sent Successfuly.' , pos: 'top-center' , status: 'success'});
            }else if(data == 2){
                UIkit.notify({message: 'Please, Login In First!' , pos: 'top-center' , status: 'danger'});
            }else{
                // UIkit.notify({message: 'Please, Fill in Your Mobile Number First!' , pos: 'top-center' , status: 'warning'});
                UIkit.modal("#mobile_modal").show();
                console.log("Ha");
            }
        }
    });
}


